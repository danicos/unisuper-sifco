USE [TEST]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Daniel Cos
-- Create date: 29/05/2019
-- Description:	Extrae los descuentos aprobados en softland y los almacena en tablas temporales
-- =============================================

CREATE PROCEDURE  [dbo].[UNI_SIFCO_PLANILLA_WB_EXTRAE]
	
AS
BEGIN
	SET NOCOUNT on;

	--Limpia la tabla temporal donde se almacenaran los datos para su procesamiento en la interfaz
	DELETE FROM dbo.UNI_SIFCO_DATOS_PLANILLA_WEBSERVICETMP;

	--Inserta en la tabla temporal los descuentos aprobados en softland
	INSERT INTO dbo.UNI_SIFCO_DATOS_PLANILLA_WEBSERVICETMP (EMPLEADO, MONTO, CONCEPTO, PLANILLA, DESCRIPCION )
	SELECT n.EMPLEADO, n.TOTAL, n.CONCEPTO, n.NOMINA, n.NUMERO_NOMINA  FROM UNISUP.EMPLEADO_CONC_NOMI n
	WHERE n.NUMERO_NOMINA = (SELECT MAX(NUMERO_NOMINA) FROM unisup.empleado_conc_nomi)
	AND n.CONCEPTO IN (
		SELECT DISTINCT CONCEPTO FROM dbo.UNI_SIFCO_DATOS_PLANILLA_WEBSERVICE
	)
	AND n.EMPLEADO IN (
		SELECT DISTINCT EMPLEADO FROM dbo.UNI_SIFCO_DATOS_PLANILLA_WEBSERVICE
	)

END
GO



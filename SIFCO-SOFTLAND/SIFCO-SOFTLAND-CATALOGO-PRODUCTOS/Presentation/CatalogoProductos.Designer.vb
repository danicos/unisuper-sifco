﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CatalogoProductos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CatalogoProductos))
        Me.lbproductos = New System.Windows.Forms.ListBox()
        Me.bsubir = New System.Windows.Forms.Button()
        Me.bbajar = New System.Windows.Forms.Button()
        Me.cbfrecuencia = New System.Windows.Forms.ComboBox()
        Me.pnlEncabezado = New System.Windows.Forms.Panel()
        Me.pnlDivisionEnca = New System.Windows.Forms.Panel()
        Me.lblEncabezado = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.bguardar = New System.Windows.Forms.Button()
        Me.beliminar = New System.Windows.Forms.Button()
        Me.pnlEncabezado.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbproductos
        '
        Me.lbproductos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbproductos.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbproductos.FormattingEnabled = True
        Me.lbproductos.ItemHeight = 20
        Me.lbproductos.Location = New System.Drawing.Point(72, 171)
        Me.lbproductos.Name = "lbproductos"
        Me.lbproductos.Size = New System.Drawing.Size(503, 262)
        Me.lbproductos.TabIndex = 0
        '
        'bsubir
        '
        Me.bsubir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bsubir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bsubir.Image = CType(resources.GetObject("bsubir.Image"), System.Drawing.Image)
        Me.bsubir.Location = New System.Drawing.Point(612, 184)
        Me.bsubir.Name = "bsubir"
        Me.bsubir.Size = New System.Drawing.Size(124, 43)
        Me.bsubir.TabIndex = 1
        Me.bsubir.Text = "Subir"
        Me.bsubir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bsubir.UseVisualStyleBackColor = True
        '
        'bbajar
        '
        Me.bbajar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bbajar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bbajar.Image = CType(resources.GetObject("bbajar.Image"), System.Drawing.Image)
        Me.bbajar.Location = New System.Drawing.Point(612, 242)
        Me.bbajar.Name = "bbajar"
        Me.bbajar.Size = New System.Drawing.Size(124, 43)
        Me.bbajar.TabIndex = 2
        Me.bbajar.Text = "Bajar"
        Me.bbajar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bbajar.UseVisualStyleBackColor = True
        '
        'cbfrecuencia
        '
        Me.cbfrecuencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbfrecuencia.FormattingEnabled = True
        Me.cbfrecuencia.Location = New System.Drawing.Point(162, 86)
        Me.cbfrecuencia.Name = "cbfrecuencia"
        Me.cbfrecuencia.Size = New System.Drawing.Size(394, 28)
        Me.cbfrecuencia.TabIndex = 3
        '
        'pnlEncabezado
        '
        Me.pnlEncabezado.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEncabezado.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.pnlEncabezado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlEncabezado.Controls.Add(Me.pnlDivisionEnca)
        Me.pnlEncabezado.Controls.Add(Me.lblEncabezado)
        Me.pnlEncabezado.Location = New System.Drawing.Point(-8, -1)
        Me.pnlEncabezado.Name = "pnlEncabezado"
        Me.pnlEncabezado.Size = New System.Drawing.Size(800, 54)
        Me.pnlEncabezado.TabIndex = 106
        '
        'pnlDivisionEnca
        '
        Me.pnlDivisionEnca.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlDivisionEnca.BackColor = System.Drawing.Color.DarkGray
        Me.pnlDivisionEnca.Location = New System.Drawing.Point(-2, 48)
        Me.pnlDivisionEnca.Name = "pnlDivisionEnca"
        Me.pnlDivisionEnca.Size = New System.Drawing.Size(804, 2)
        Me.pnlDivisionEnca.TabIndex = 14
        '
        'lblEncabezado
        '
        Me.lblEncabezado.AutoSize = True
        Me.lblEncabezado.BackColor = System.Drawing.Color.Transparent
        Me.lblEncabezado.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEncabezado.ForeColor = System.Drawing.Color.White
        Me.lblEncabezado.Location = New System.Drawing.Point(17, 13)
        Me.lblEncabezado.Name = "lblEncabezado"
        Me.lblEncabezado.Size = New System.Drawing.Size(350, 23)
        Me.lblEncabezado.TabIndex = 1
        Me.lblEncabezado.Text = "PRIORIDAD DE PRODUCTOS SIFCO"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(68, 89)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 20)
        Me.Label1.TabIndex = 107
        Me.Label1.Text = "Frecuencia"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(68, 138)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 20)
        Me.Label2.TabIndex = 108
        Me.Label2.Text = "Productos"
        '
        'bguardar
        '
        Me.bguardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bguardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bguardar.Image = CType(resources.GetObject("bguardar.Image"), System.Drawing.Image)
        Me.bguardar.Location = New System.Drawing.Point(612, 359)
        Me.bguardar.Name = "bguardar"
        Me.bguardar.Size = New System.Drawing.Size(124, 43)
        Me.bguardar.TabIndex = 109
        Me.bguardar.Text = "Guardar"
        Me.bguardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bguardar.UseVisualStyleBackColor = True
        '
        'beliminar
        '
        Me.beliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.beliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beliminar.Image = CType(resources.GetObject("beliminar.Image"), System.Drawing.Image)
        Me.beliminar.Location = New System.Drawing.Point(612, 301)
        Me.beliminar.Name = "beliminar"
        Me.beliminar.Size = New System.Drawing.Size(124, 43)
        Me.beliminar.TabIndex = 110
        Me.beliminar.Text = "Eliminar"
        Me.beliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.beliminar.UseVisualStyleBackColor = True
        '
        'CatalogoProductos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(784, 462)
        Me.Controls.Add(Me.beliminar)
        Me.Controls.Add(Me.bguardar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.pnlEncabezado)
        Me.Controls.Add(Me.cbfrecuencia)
        Me.Controls.Add(Me.bbajar)
        Me.Controls.Add(Me.bsubir)
        Me.Controls.Add(Me.lbproductos)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "CatalogoProductos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SIFCO-SOFTLAND"
        Me.pnlEncabezado.ResumeLayout(False)
        Me.pnlEncabezado.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbproductos As ListBox
    Friend WithEvents bsubir As Button
    Friend WithEvents bbajar As Button
    Friend WithEvents cbfrecuencia As ComboBox
    Friend WithEvents pnlEncabezado As Panel
    Friend WithEvents pnlDivisionEnca As Panel
    Friend WithEvents lblEncabezado As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents bguardar As Button
    Friend WithEvents beliminar As Button
End Class

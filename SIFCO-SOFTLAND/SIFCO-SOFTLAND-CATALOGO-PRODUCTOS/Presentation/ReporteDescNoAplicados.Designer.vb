﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReporteDescNoAplicados
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReporteDescNoAplicados))
        Me.pnlEncabezado = New System.Windows.Forms.Panel()
        Me.pnlDivisionEnca = New System.Windows.Forms.Panel()
        Me.lblEncabezado = New System.Windows.Forms.Label()
        Me.dgvnominas = New System.Windows.Forms.DataGridView()
        Me.dgvdescuentos = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lIndexGrid = New System.Windows.Forms.Label()
        Me.button3 = New System.Windows.Forms.Button()
        Me.button4 = New System.Windows.Forms.Button()
        Me.button2 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.bdescargar = New System.Windows.Forms.Button()
        Me.sfdDescarga = New System.Windows.Forms.SaveFileDialog()
        Me.pnlEncabezado.SuspendLayout()
        CType(Me.dgvnominas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvdescuentos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlEncabezado
        '
        Me.pnlEncabezado.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEncabezado.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.pnlEncabezado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlEncabezado.Controls.Add(Me.pnlDivisionEnca)
        Me.pnlEncabezado.Controls.Add(Me.lblEncabezado)
        Me.pnlEncabezado.Location = New System.Drawing.Point(-7, -1)
        Me.pnlEncabezado.Name = "pnlEncabezado"
        Me.pnlEncabezado.Size = New System.Drawing.Size(812, 54)
        Me.pnlEncabezado.TabIndex = 107
        '
        'pnlDivisionEnca
        '
        Me.pnlDivisionEnca.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlDivisionEnca.BackColor = System.Drawing.Color.DarkGray
        Me.pnlDivisionEnca.Location = New System.Drawing.Point(-2, 48)
        Me.pnlDivisionEnca.Name = "pnlDivisionEnca"
        Me.pnlDivisionEnca.Size = New System.Drawing.Size(816, 2)
        Me.pnlDivisionEnca.TabIndex = 14
        '
        'lblEncabezado
        '
        Me.lblEncabezado.AutoSize = True
        Me.lblEncabezado.BackColor = System.Drawing.Color.Transparent
        Me.lblEncabezado.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEncabezado.ForeColor = System.Drawing.Color.White
        Me.lblEncabezado.Location = New System.Drawing.Point(17, 13)
        Me.lblEncabezado.Name = "lblEncabezado"
        Me.lblEncabezado.Size = New System.Drawing.Size(427, 23)
        Me.lblEncabezado.TabIndex = 1
        Me.lblEncabezado.Text = "REPORTE DE DESCUENTOS NO APLICADOS"
        '
        'dgvnominas
        '
        Me.dgvnominas.AllowUserToAddRows = False
        Me.dgvnominas.AllowUserToDeleteRows = False
        Me.dgvnominas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvnominas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvnominas.Location = New System.Drawing.Point(16, 106)
        Me.dgvnominas.MultiSelect = False
        Me.dgvnominas.Name = "dgvnominas"
        Me.dgvnominas.ReadOnly = True
        Me.dgvnominas.Size = New System.Drawing.Size(272, 301)
        Me.dgvnominas.TabIndex = 108
        '
        'dgvdescuentos
        '
        Me.dgvdescuentos.AllowUserToAddRows = False
        Me.dgvdescuentos.AllowUserToDeleteRows = False
        Me.dgvdescuentos.AllowUserToOrderColumns = True
        Me.dgvdescuentos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvdescuentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvdescuentos.Location = New System.Drawing.Point(324, 106)
        Me.dgvdescuentos.Name = "dgvdescuentos"
        Me.dgvdescuentos.Size = New System.Drawing.Size(464, 301)
        Me.dgvdescuentos.TabIndex = 109
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 83)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 20)
        Me.Label1.TabIndex = 110
        Me.Label1.Text = "Nómina"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(320, 83)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(192, 20)
        Me.Label2.TabIndex = 111
        Me.Label2.Text = "Descuentos No Aplicados"
        '
        'lIndexGrid
        '
        Me.lIndexGrid.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.lIndexGrid.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.lIndexGrid.Location = New System.Drawing.Point(131, 417)
        Me.lIndexGrid.Name = "lIndexGrid"
        Me.lIndexGrid.Size = New System.Drawing.Size(42, 22)
        Me.lIndexGrid.TabIndex = 117
        Me.lIndexGrid.Text = "/"
        Me.lIndexGrid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'button3
        '
        Me.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.button3.Location = New System.Drawing.Point(207, 417)
        Me.button3.Name = "button3"
        Me.button3.Size = New System.Drawing.Size(31, 23)
        Me.button3.TabIndex = 116
        Me.button3.Text = ">>"
        Me.button3.UseVisualStyleBackColor = True
        '
        'button4
        '
        Me.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.button4.Location = New System.Drawing.Point(179, 417)
        Me.button4.Name = "button4"
        Me.button4.Size = New System.Drawing.Size(22, 23)
        Me.button4.TabIndex = 115
        Me.button4.Text = ">"
        Me.button4.UseVisualStyleBackColor = True
        '
        'button2
        '
        Me.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.button2.Location = New System.Drawing.Point(66, 417)
        Me.button2.Name = "button2"
        Me.button2.Size = New System.Drawing.Size(31, 23)
        Me.button2.TabIndex = 114
        Me.button2.Text = "<<"
        Me.button2.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Button5.Location = New System.Drawing.Point(103, 417)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(22, 23)
        Me.Button5.TabIndex = 113
        Me.Button5.Text = "<"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'bdescargar
        '
        Me.bdescargar.Enabled = False
        Me.bdescargar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bdescargar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.bdescargar.Image = CType(resources.GetObject("bdescargar.Image"), System.Drawing.Image)
        Me.bdescargar.Location = New System.Drawing.Point(687, 417)
        Me.bdescargar.Name = "bdescargar"
        Me.bdescargar.Size = New System.Drawing.Size(101, 37)
        Me.bdescargar.TabIndex = 118
        Me.bdescargar.Text = "Descargar"
        Me.bdescargar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bdescargar.UseVisualStyleBackColor = True
        '
        'sfdDescarga
        '
        Me.sfdDescarga.DefaultExt = "csv"
        Me.sfdDescarga.Filter = "CSV files (*.csv)|"
        '
        'ReporteDescNoAplicados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(800, 466)
        Me.Controls.Add(Me.bdescargar)
        Me.Controls.Add(Me.lIndexGrid)
        Me.Controls.Add(Me.button3)
        Me.Controls.Add(Me.button4)
        Me.Controls.Add(Me.button2)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvdescuentos)
        Me.Controls.Add(Me.dgvnominas)
        Me.Controls.Add(Me.pnlEncabezado)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ReporteDescNoAplicados"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SIFCO-SOFTLAND"
        Me.pnlEncabezado.ResumeLayout(False)
        Me.pnlEncabezado.PerformLayout()
        CType(Me.dgvnominas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvdescuentos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnlEncabezado As Panel
    Friend WithEvents pnlDivisionEnca As Panel
    Friend WithEvents lblEncabezado As Label
    Friend WithEvents dgvnominas As DataGridView
    Friend WithEvents dgvdescuentos As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Private WithEvents lIndexGrid As Label
    Private WithEvents button3 As Button
    Private WithEvents button4 As Button
    Private WithEvents button2 As Button
    Private WithEvents Button5 As Button
    Private WithEvents bdescargar As Button
    Friend WithEvents sfdDescarga As SaveFileDialog
End Class

﻿
Public Class CatalogoProductos
    Dim productos As List(Of Producto)
    Private Sub Bsubir_Click(sender As Object, e As EventArgs) Handles bsubir.Click
        Try
            Dim index = lbproductos.SelectedIndex
            If index > 0 Then
                lbproductos.DataSource = Nothing
                Dim item = productos(index)
                productos.RemoveAt(index)
                productos.Insert(index - 1, item)
                lbproductos.DataSource = productos
                lbproductos.DisplayMember = "Nombre"
                lbproductos.SelectedIndex = index - 1
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Ocurrio un problema")
        End Try
    End Sub

    Private Sub Bbajar_Click(sender As Object, e As EventArgs) Handles bbajar.Click
        Try
            Dim index = lbproductos.SelectedIndex
            If index < (lbproductos.Items.Count - 1) Then
                lbproductos.DataSource = Nothing
                Dim item = productos(index)
                productos.RemoveAt(index)
                productos.Insert(index + 1, item)
                lbproductos.DataSource = productos
                lbproductos.DisplayMember = "Nombre"
                lbproductos.SelectedIndex = index + 1
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Ocurrio un problema")
        End Try
    End Sub

    Private Sub CatalogoProductos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Dim dboperations = New DBOperations()
            Dim frecuencias = dboperations.selectFrecuencias()
            dboperations.Close()
            cbfrecuencia.DataSource = frecuencias
            cbfrecuencia.DisplayMember = "Nombre"
            cbfrecuencia.ValueMember = "Nombre"
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Ocurrio un problema")
        End Try
    End Sub

    Private Sub CargarProductos()
        Try
            Dim frecuencia = cbfrecuencia.SelectedItem()
            If frecuencia Is Nothing Then
                Return
            End If
            Dim dboperations = New DBOperations()
            productos = dboperations.selectProductos(frecuencia.Nombre)
            dboperations.Close()
            lbproductos.DataSource = productos
            lbproductos.DisplayMember = "Nombre"
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Ocurrio un problema")
        End Try
    End Sub
    Private Sub Cbfrecuencia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbfrecuencia.SelectedIndexChanged
        CargarProductos()
    End Sub

    Private Sub Bguardar_Click(sender As Object, e As EventArgs) Handles bguardar.Click
        Try
            Dim dboperations = New DBOperations()
            For i As Integer = 1 To productos.Count Step 1
                dboperations.updatePrioridadProducto(productos(i - 1), i)
            Next
            dboperations.Close()
            MessageBox.Show("Guardado exitosamente.", "UNI-SIFCO")
        Catch ex As exception
            MessageBox.Show(ex.Message, "Ocurrio un problema")
        End Try
    End Sub

    Private Sub CatalogoProductos_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Owner.Show()
    End Sub

    Private Sub Beliminar_Click(sender As Object, e As EventArgs) Handles beliminar.Click
        Try
            If MessageBox.Show("¿Desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.OKCancel) = DialogResult.OK Then
                Dim producto As Producto = lbproductos.SelectedItem
                Dim dboperations = New DBOperations()
                dboperations.deleteProducto(producto.Nombre)
                dboperations.Close()
                CargarProductos()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Ocurrio un problema")
        End Try
    End Sub
End Class

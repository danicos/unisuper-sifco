﻿Imports System.IO
Imports System.Text

Public Class ReporteDescNoAplicados
    Private Const _NOROWS = 15
    Private _INDEX = 0
    Private _MAXVAL = 0
    Dim csv As StringBuilder
    Private Sub ReporteDescNoAplicados_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        setIndexMax()
        cargarNominas()
    End Sub

    Private Sub ReporteDescNoAplicados_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Owner.Show()
    End Sub

    Private Sub Dgvnominas_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvnominas.CellDoubleClick
        Try
            Dim dboperations = New DBOperations()
            Dim planilla = dgvnominas.Rows(e.RowIndex).Cells(0).Value.ToString()
            Dim numero = dgvnominas.Rows(e.RowIndex).Cells(1).Value.ToString()
            Dim concepto = dgvnominas.Rows(e.RowIndex).Cells(2).Value.ToString()
            Dim dataset As DataSet = dboperations.selectDescuentos(planilla, numero, concepto)
            dgvdescuentos.DataSource = If(dataset.Tables Is Nothing, Nothing, dataset.Tables(0))
            dboperations.Close()
            llenarCsv(planilla, numero, concepto, dataset)
            bdescargar.Enabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Ocurrio un problema")
        End Try
    End Sub

    Private Sub cargarNominas()
        Try
            Dim dboperations = New DBOperations()
            Dim dataset As DataSet = dboperations.selectNominas(_INDEX, _NOROWS)
            dgvnominas.DataSource = If(dataset.Tables Is Nothing, Nothing, dataset.Tables(0))
            dboperations.Close()
            lIndexGrid.Text = (_INDEX + 1).ToString() + "/" + (_MAXVAL + 1).ToString()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Ocurrio un problema")
        End Try
    End Sub

    Private Sub setIndexMax()
        Try
            Dim dboperations = New DBOperations()
            Dim count As Integer = dboperations.countNominas()
            If count > 0 Then
                Dim result As Double = Double.Parse(count) / Double.Parse(_NOROWS)
                _MAXVAL = Integer.Parse(Math.Ceiling(result))
                _MAXVAL -= 1
            Else
                _MAXVAL = 0
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Ocurrio un problema")
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles button2.Click
        _INDEX = 0
        cargarNominas()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If _INDEX > 0 Then
            _INDEX -= 1
            cargarNominas()
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles button4.Click
        If _INDEX < _MAXVAL Then
            _INDEX += 1
            cargarNominas()
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles button3.Click
        _INDEX = _MAXVAL
        cargarNominas()
    End Sub

    Private Sub Bdescargar_Click(sender As Object, e As EventArgs) Handles bdescargar.Click
        If sfdDescarga.ShowDialog() = DialogResult.OK Then
            CrearReporteCSV(sfdDescarga.FileName)
        End If
    End Sub

    Public Sub CrearReporteCSV(filename As String)
        File.WriteAllText(filename, csv.ToString())
    End Sub

    Public Sub llenarCsv(planilla As String, numero As String, concepto As String, dataset As DataSet)
        csv = New StringBuilder()
        Dim newLine As String = String.Format("{0},{1},{2},{3},{4}",
                "Nomina",
                "Numero Nomina",
                "Concepto",
                "Empleado",
                "Monto"
                )
        csv.AppendLine(newLine)
        If Not dataset Is Nothing Then
            For Each line In dataset.Tables(0).Rows
                newLine = String.Format("{0},{1},{2},{3},{4}", planilla, numero, concepto, line(0), line(1))
                csv.AppendLine(newLine)
            Next
        End If

    End Sub

End Class
﻿Imports System.Reflection
Imports Conexion_sqlserver

Public Class DBOperations

    Private conexion As clsConexionSqlServer
    Private Const _NAME_XMLFILE_DB As String = "cnSQL.xml"
    Public Sub New()
        conexion = New clsConexionSqlServer()
        conexion.BuscarConexionEnXml(_NAME_XMLFILE_DB, Application.StartupPath)
    End Sub
    Public Function selectFrecuencias()
        Try
            Dim ds = conexion.llenarDataSet(DBQuery._Q_SELECT_FRECUENCIAS)
            Dim res As List(Of Frecuencia) = ConvertDataTable(Of Frecuencia)(ds.Tables(0))
            Return res
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function selectProductos(frecuencia As String)
        Try
            Dim ds = conexion.llenarDataSet(String.Format(DBQuery._Q_SELECT_PRODUCTOS, frecuencia))
            Dim res As List(Of Producto) = ConvertDataTable(Of Producto)(ds.Tables(0))
            Return res
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function deleteProducto(name As String)
        Try
            conexion.EjecutarNonQuery(String.Format(DBQuery._Q_DELETE_PRODUCTO, name))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function updatePrioridadProducto(producto As Producto, prioridad As Integer)
        Try
            conexion.EjecutarNonQuery(String.Format(DBQuery._Q_UPDATE_PRODUCTO, prioridad.ToString(), producto.Nombre))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function selectNominas(index As Integer, norows As Integer)
        Try
            Return conexion.llenarDataSet(String.Format(DBQuery._Q_SELECT_NOMINAS, (index * norows), norows))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function countNominas()
        Try
            Return conexion.EjecutarEscalar(DBQuery._Q_SELECT_COUNT_NOMINAS)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function selectDescuentos(planilla As String, numero As String, concepto As String)
        Try
            Return conexion.llenarDataSet(String.Format(DBQuery._Q_SELECT_DESCUENTOS, planilla, numero, concepto))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub Close()
        conexion.CerrarConexion()
    End Sub
    Private Function ConvertDataTable(Of T)(dt As DataTable) As List(Of T)
        Dim data As List(Of T) = New List(Of T)()
        For Each row As DataRow In dt.Rows
            Dim item As T = GetItem(Of T)(row)
            data.Add(item)
        Next
        Return data
    End Function
    Private Function GetItem(Of T)(dr As DataRow) As T
        Dim temp As Type = GetType(T)
        Dim obj As T = Activator.CreateInstance(Of T)()
        For Each column As DataColumn In dr.Table.Columns
            For Each pro As PropertyInfo In temp.GetProperties()
                If pro.Name = column.ColumnName Then
                    pro.SetValue(obj, dr(column.ColumnName).ToString(), Nothing)
                Else
                    Continue For
                End If
            Next
        Next
        Return obj
    End Function
End Class

Public Class DBQuery
    Public Const _Q_SELECT_FRECUENCIAS As String = "SELECT DISTINCT FRECUENCIA AS Nombre FROM dbo.UNI_SIFCO_PRODUCTO"
    Public Const _Q_SELECT_PRODUCTOS As String = "SELECT NOMBRE AS Nombre, PRIORIDAD AS Prioridad FROM dbo.UNI_SIFCO_PRODUCTO WHERE FRECUENCIA = '{0}' ORDER BY PRIORIDAD ASC"
    Public Const _Q_DELETE_PRODUCTO As String = "DELETE FROM dbo.UNI_SIFCO_PRODUCTO WHERE NOMBRE = '{0}'"
    Public Const _Q_UPDATE_PRODUCTO As String = "UPDATE dbo.UNI_SIFCO_PRODUCTO SET PRIORIDAD = '{0}' WHERE NOMBRE = '{1}'"
    Public Const _Q_SELECT_NOMINAS As String = "SELECT DISTINCT NOMINA AS Nomina, NUMERO_NOMINA AS 'Numero Nomina', CONCEPTO as Concepto FROM dbo.UNI_SIFCO_DESCUENTO_NO_APLICADO ORDER BY NOMINA DESC, NUMERO_NOMINA DESC, CONCEPTO DESC OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY"
    Public Const _Q_SELECT_DESCUENTOS As String = "SELECT EMPLEADO AS Empleado, MONTO AS Monto FROM dbo.UNI_SIFCO_DESCUENTO_NO_APLICADO WHERE NOMINA = '{0}' AND NUMERO_NOMINA = '{1}' AND CONCEPTO = '{2}'"
    Public Const _Q_SELECT_COUNT_NOMINAS As String = "SELECT COUNT(*) FROM (SELECT  DISTINCT NOMINA, NUMERO_NOMINA, CONCEPTO  FROM dbo.UNI_SIFCO_DESCUENTO_NO_APLICADO) AS n"
End Class

Public Class Frecuencia
    Public Property Nombre As String
End Class

Public Class Producto
    Public Property Nombre As String
    Public Property Prioridad As String
End Class
﻿Imports System
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Web.Script.Serialization
Imports System.Threading

Public Class CargaMasiva
    Dim contador As Integer
    Dim tabla As DataSet
    Dim resultado As Integer
    Dim resultado2 As Integer
    Dim FIN As Integer
    Dim fs1 As FileStream = New FileStream(Application.StartupPath &
            "\LogCarga.txt", FileMode.Append, FileAccess.Write)
    Dim s1 As StreamWriter = New StreamWriter(fs1)
    Dim mensaje As String
    Dim tabla2 As New DataSet


    Public Function CargaEmpleados()

        Try
            Dim URI As Uri = New Uri(urlcarga)
            Dim jsonString As String = String.Empty


            Dim encabezadoJson As String = "{
          ""CargasMasivasAutomaticasFormatoID"": " & CMAFormatoID & ",
          ""CargasMasivasArchivoTipoCarga"": " & CMArchivoTC & ",
          ""CargasMasivasWSCampos_SDT"": [ " & vbCrLf
            Dim encabezadoFila As String = "{
              ""PersonaCampos"": [ " & vbCrLf

            Dim finFila As String = "      ]
            } "
            Dim finJson As String = vbCrLf & "          ]
        }"
            Dim valores As String = String.Empty
            Dim icontador As Integer = 0
            Dim contador As Integer

            'Pantalla de espera del la respuesta

            ' PEspera1.Visible = True

            'Await Task.Delay(2000)
            ' For Each fila2 As DataRow In tabla.Tables(0).Rows

            Dim MaxRowGroup As Integer = 500

            For Each row As DataRow In tabla.Tables(0).Rows
                contador += 1
                'dtgDatos.Columns.Count -1
                If icontador > 0 Then
                    valores &= ", " & vbCrLf
                End If

                valores &= encabezadoFila
                For i As Integer = 0 To tabla.Tables(0).Columns.Count - 1
                    'se incluye una coma despues de la primer columna...
                    If i > 0 Then
                        valores &= ", " & vbCrLf
                    End If
                    valores = valores & "{""Valor"": """ & row.Item(i).ToString.Replace("""", String.Empty).Replace(vbCr, String.Empty) & """}"


                Next
                valores &= finFila
                icontador += 1
                If contador = MaxRowGroup Then
                    Dim ContentFile As String = encabezadoJson & valores & finJson
                    ''aqui envias al web service y escrbis en el archivo como existoso
                    Dim data = Encoding.UTF8.GetBytes(ContentFile)


                    'Dim result_post = Await 
                    SendRequest(URI, data, "application/json", "POST")

                    resultado2 = 1
                    contador = 0
                    valores = ""
                    icontador = 0
                End If


            Next
            If contador < MaxRowGroup Then
                Dim ContentFile As String = encabezadoJson & valores & finJson
                ''aqui envias al web service y escrbis en el archivo como existoso
                Dim data = Encoding.UTF8.GetBytes(ContentFile)


                'Dim result_post = Await 
                SendRequest(URI, data, "application/json", "POST")

                resultado2 = 1
                contador = 0
                valores = ""
                icontador = 0
            End If
            If tabla.Tables(0).Rows.Count = 0 Then
                resultado2 = 2
            End If
            'If dtgDatos.Rows.Count > 0 Then
            '    'Recorremos fila por fila para obtener los valores

            '    For Each fila As DataGridViewRow In dtgDatos.Rows



            '        'For Each fila2 As DataRow In tabla2.Tables(0).Rows


            '        ' Next
            '        'le damos el encabezado a la fila.



            '        If icontador > 0 Then
            '            valores &= ", " & vbCrLf
            '        End If

            '        valores &= encabezadoFila

            '        'recorremos columna por columna por cada fila
            '        For i As Integer = 0 To dtgDatos.Columns.Count - 1
            '            'se incluye una coma despues de la primer columna...
            '            If i > 0 Then
            '                valores &= ", " & vbCrLf
            '            End If

            '            valores = valores & "{""Valor"": """ & fila.Cells(i).Value.ToString.Replace("""", String.Empty).Replace(vbCr, String.Empty) & """}"


            '        Next

            '        valores &= finFila

            '        'aumentamos contador...
            '        icontador += 1

            '        ' If icontador >= 500 Then


            '        'icontador = 0
            '        'ElseIf icontador = tabla.Tables(0).Rows.Count Then
            '        '    jsonString = encabezadoJson & valores & finJson
            '        '    Dim data = Encoding.UTF8.GetBytes(jsonString)
            '        '    'Dim result_post = Await 
            '        '    SendRequest(URI, data, "application/json", "POST")
            '        '    conexion.timeOutCommand = 200000
            '        '    resultado2 = 1

            '        'End If



            '    Next

            'jsonString = encabezadoJson & valores & finJson
            'Dim data = Encoding.UTF8.GetBytes(jsonString)


            '    'Dim result_post = Await 
            '    SendRequest(URI, data, "application/json", "POST")

            '    resultado2 = 1



            'Else

            '    resultado2 = 2
            'End If

        Catch ex As Exception
            _errorJSON = ex.Message
            resultado2 = 0
        End Try
        Return resultado2

    End Function

    Private _errorJSON As String

    Private Sub SendRequest(uri As Uri, jsonDataBytes As Byte(), contentType As String, method As String) 'As Task(Of String)

        Dim req As WebRequest = WebRequest.Create(uri)
        'Cargando.ShowDialog()
        req.ContentType = contentType
        req.Headers("Authorization") = access_token
        req.Headers("GENEXUS-AGENT") = agente
        req.Method = method
        req.ContentLength = jsonDataBytes.Length

        Try
            Dim stream = req.GetRequestStream()
            stream.Write(jsonDataBytes, 0, jsonDataBytes.Length)

            stream.Close()
            'req.Timeout = 1000000

            '  conexion.timeOutCommand = 200000
        Catch ex As Exception

            s1.Write("Titulo: " & "Error de Servidor" & vbCrLf)
            s1.Write("Mensaje: " & ex.Message & vbCrLf)
            s1.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s1.Write("=====================================================" & vbCrLf)
            s1.Close()
            fs1.Close()
            End
        End Try
        Dim res As String
        Dim response As Stream = req.GetResponse().GetResponseStream()

        Dim reader As New StreamReader(response)
        res = reader.ReadToEnd()
        reader.Close()
        response.Close()


    End Sub

    Private Sub CargaMasiva_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Label3.Text = "Versión " & strVersion

        Conexion_Base()
        '**********************************Boton de bajar de softland****************************************************************
        BotonBajarSoftland()
        '****************************************************************************************************************************
        '**********************************Boton de Subir a Web Service**************************************************************
        If FIN = 1 Then
            End
        Else
            CargaWS()
            End
        End If

        '****************************************************************************************************************************
        End
    End Sub
    Private Function Descarga()
        Dim q As Integer
        'Try.. catch para la carga en el datagridview
        Try

            tabla = conexion.llenarDataSet("Exec UNI_SIFCO_EMPLEADOS;")
            'tabla = tabla2.Clone
            'For q = 0 To 500
            '    tabla.Tables(0).ImportRow(tabla2.Tables(0).Rows(q))

            'Next
            If tabla.Tables(0).Rows.Count = 0 Then
                resultado = 2
            Else

                resultado = 1
            End If

        Catch ex As Exception
            _errorJSON = ex.Message
            resultado = 0

        End Try
        Return resultado
    End Function

    Private Sub BotonBajarSoftland()
        Dim MENSAJE As String

        Descarga()



        If resultado = 0 Then
            'Mensaje a log
            ' My.Computer.FileSystem.WriteAllText(Application.StartupPath & "\Log.txt", _errorJSON & vbNewLine, True)


            s1.Write("Titulo: " & "Error" & vbCrLf)
            s1.Write("Mensaje: " & _errorJSON & vbCrLf)
            s1.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s1.Write("=====================================================" & vbCrLf)
            s1.Close()
            fs1.Close()
            End

        ElseIf resultado = 1 Then
            dtgDatos.DataSource = tabla.Tables(0)

            MENSAJE = tabla.Tables(0).Rows.Count & " Colaboradores cargados. "


            s1.Write("Titulo: " & "Descarga Existosa" & vbCrLf)
            s1.Write("Mensaje: " & MENSAJE & vbCrLf)
            s1.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s1.Write("=====================================================" & vbCrLf)
            's1.Close()
            'fs1.Close()
        ElseIf resultado = 2 Then
            dtgDatos.DataSource = ""
            'Mensaje a log
            MENSAJE = "No hay datos nuevos"
            'MsgBox("No hay datos nuevos", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
            'My.Computer.FileSystem.WriteAllText(Application.StartupPath & "\Log.txt", MENSAJE & vbNewLine, True)
            s1.Write("Titulo: " & "Informativo" & vbCrLf)
            s1.Write("Mensaje: " & MENSAJE & vbCrLf)
            s1.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s1.Write("=====================================================" & vbCrLf)
            s1.Close()
            fs1.Close()
            End
            FIN = 1
        End If
    End Sub


    Private Sub CargaWS()

        'Se llama a la Funcion Carga empleados

        CargaEmpleados()

        If resultado2 = 0 Then
            'A log de errores
            'MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")
            s1.Write("Titulo: " & "Error" & vbCrLf)
            s1.Write("Mensaje: " & _errorJSON & vbCrLf)
            s1.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s1.Write("=====================================================" & vbCrLf)
            s1.Close()
            fs1.Close()
            End
        ElseIf resultado2 = 1 Then
            'A log de errores

            ' MsgBox(dtgDatos.Rows.Count & " Colaboradores cargados. ", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
            mensaje = dtgDatos.Rows.Count & " Colaboradores cargados. "
            s1.Write("Titulo: " & "Carga Exitosa" & vbCrLf)
            s1.Write("Mensaje: " & mensaje & vbCrLf)
            s1.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s1.Write("=====================================================" & vbCrLf)
            s1.Close()
            fs1.Close()

        ElseIf resultado2 = 2 Then
            'A log de errores
            mensaje = "No hay datos a enviar"

            s1.Write("Titulo: " & "Informativo" & vbCrLf)
            s1.Write("Mensaje: " & mensaje & vbCrLf)
            s1.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s1.Write("=====================================================" & vbCrLf)
            s1.Close()
            fs1.Close()
            End
        End If
    End Sub
    Dim passwordt As String
    Private Sub Conexion_Base()

        Try
            conexion.BuscarConexionEnXml("cnSQL.xml", Application.StartupPath)

            ClientId = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'ClientId'")
            ClientSecret = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'ClientSecret'")
            Grant_Type = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'Grant_Type'")
            Scope = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'Scope'")
            Username = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'Username'")
            passwordt = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'Password'")
            Password = Crypto.Crypto.DesEncriptar(passwordt)
            urlautenticacion = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlAutenticacion'")
            urlcarga = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlCarga'")
            urldescargadescuentos = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlDescargaDescuento'")
            urlcargaplanilla = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlCargaPlanilla'")
            urlcambioestado = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlCambioEstado'")
            urlconsultaemail = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlConsultaEmail'")
            CMAFormatoID = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'CMAFormatoID'")
            CMArchivoTC = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'CMArchivoTC'")
            FormaIdentificar = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'FormaIdentificar'")
            OperacionAct = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'OperacionAct'")
            OperacionInac = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'OperacionInac'")
            agente = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'GENEXUS-AGENT'")
            autenticacion()

        Catch ex As Exception
            'Si no hay informacion en la tabla de la autenticacion, manda un mensaje
            'MsgBox(ex.Message, MsgBoxStyle.Critical, "SIFCO - SOFTLAND")
            s1.Write("Titulo: " & "Error de Conexión BD" & vbCrLf)
            s1.Write("Mensaje: " & ex.Message & vbCrLf)
            s1.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s1.Write("=====================================================" & vbCrLf)
            s1.Close()
            fs1.Close()
            End
        End Try
    End Sub

    Sub InsertData(ByVal dt As DataTable)

        Dim encabezado As String = "inicio de archivo"
        Dim finish As String = "final de archivo"
        Dim valores As String = ""
        Dim Contador As Integer = 0
        Dim MaxRowGroup As Integer = 150

        For Each row As DataRow In dt.Rows
            Contador += 1
            valores = valores & "{" & row.Item(0) & "}"
            If Contador = MaxRowGroup Then
                Dim ContentFile As String = encabezado & valores & finish
                ''aqui envias al web service y escrbis en el archivo como existoso
                Contador = 0
                valores = ""
            End If

        Next

        If Contador < 0 Then
            Dim ContentFile As String = encabezado & valores & finish
            ''aqui envias al web service y escrbis en el archivo como existoso
            Contador = 0
            valores = ""
        End If


    End Sub

End Class

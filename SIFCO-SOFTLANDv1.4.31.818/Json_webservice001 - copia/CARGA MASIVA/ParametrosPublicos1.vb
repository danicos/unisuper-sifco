﻿Imports System
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Web.Script.Serialization

Module ParametrosPublicos1
    Dim Usuario As String
    Dim Contraseña As String

    'Variables para la autenticacion
    Public ClientId As String
    Public ClientSecret As String
    Public Grant_Type As String
    Public Scope As String
    Public Username As String
    Public Password As String
    Public access_token As String
    Public agente As String
    Public refresh_token As String
    Public user_guid As String

    'Urls para la autenticacion y la carga
    Public urlautenticacion As String
    Public urlcarga As String
    Public urldescargadescuentos As String
    Public urlcargaplanilla As String
    Public urlcambioestado As String
    Public urlconsultaemail As String
    ' Public urlcamposdefinidos As String

    'Variables para carga Empleados
    Public CMAFormatoID As String 'Cargas Masivas Automaticas Formato ID
    Public CMArchivoTC As String 'Carga Masiva Archivos Tiipo Carga

    'Variables para la descarga de planilla

    'Planilla

    Public id As String 'ID de la planilla
    ' Public idc As Integer 
    Public Descripcion As String
    Public Fecha As String
    Public FrecuenciaCodigo As String
    Public FrecuenciaNombre As String

    'Detalle

    Public idDeta As String
    Public ClienteCodigoSifco As String
    Public ClienteReferencia As String
    Public TransaccionNumero As String
    Public TransaccionReferencia As String
    Public Monto As String

    'Public Concepto As String
    'Public Parametro As String

    ' variables para invocar las planillas
    Public FechaDesde As String
    Public FechaHasta As String

    Public errores As String
    Public mensaje As String

    'variables para el cambio de estado
    Public OperacionAct As String
    Public OperacionInac As String
    Public FormaIdentificar As String
    Public Identificador As String

    Public Type As String
    Public Idm As String
    Public Description As String

    'Variable para la consulta de email
    Public CodigoC As String
    Public Nombre As String
    Public CodigoReferencia As String
    Public EmailPrincipal As String
    Public EmailSecundario As String
    Public ExcluirMensajes As String

    Public Delegate Sub Pantalla()
    Public Delegate Sub delegado()

    'Versionamiento
    Public strLocation As String = System.Reflection.Assembly.GetExecutingAssembly.Location
    Public strVersion As String = System.Diagnostics.FileVersionInfo.GetVersionInfo(strLocation).FileVersion
    'strLocation = System.Reflection.Assembly.GetExecutingAssembly.Location
    'strVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(strLocation).FileVersion


    'variables para la carga de las planillas
    ''Public DescripcionPlanilla As String
    ''Public ErrorPlanilla As Integer
    ''Public FrecuenciaPlanillas As Integer


    Public conexion As New Conexion_sqlserver.clsConexionSqlServer


    Public Sub autenticacion()
        'aqui tenemos la direccion a donde nos vamos a conectar
        Dim URI As String = urlautenticacion
        'esto son los parametros necesarios para el logueo.
        Dim parametros As New System.Collections.Specialized.NameValueCollection
        parametros.Add("client_id", ClientId)
        parametros.Add("client_secret", ClientSecret)
        parametros.Add("grant_type", Grant_Type)
        parametros.Add("scope", Scope)
        parametros.Add("username", Username)
        parametros.Add("password", Password)
        Using wc As New WebClient()
            'enviamos la peticion
            Dim respuestabytes As Byte() = wc.UploadValues(URI, "POST", parametros)
            'recibimos la respuesta.
            Dim respuesta As String = Encoding.UTF8.GetString(respuestabytes)

            Dim json As New JavaScriptSerializer
            'tratamos de descomponer la respuesta a string...
            Dim respuestaDescompuesta1 As Object = json.DeserializeObject(respuesta)

            For Each fila In respuestaDescompuesta1

                'Key: es el nombre de la variable que estamos recuperando....
                Select Case fila.key
                    Case "access_token"
                        access_token = "OAuth " & fila.value
                    Case "scope"
                        Scope = fila.value
                    Case "refresh_token"
                        refresh_token = fila.value
                    Case "user_guid"
                        user_guid = fila.value
                End Select

            Next
        End Using

    End Sub

End Module

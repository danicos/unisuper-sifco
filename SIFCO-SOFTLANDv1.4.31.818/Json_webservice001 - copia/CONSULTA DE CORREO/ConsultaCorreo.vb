﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Web.Script.Serialization

Public Class ConsultaCorreo
    Dim tabla As New DataTable
    Private _errorJSON As String
    Private _errorJSON2 As String
    Dim resultado As Integer
    Dim resultado2 As Integer
    Dim mensaje As String
    Dim fs4 As FileStream = New FileStream(Application.StartupPath &
            "\LogConsultaEmail.txt", FileMode.Append, FileAccess.Write)
    Dim s4 As StreamWriter = New StreamWriter(fs4)
    Dim FIN As Integer
    Private Function RecorreCadena(respuesta As Object)

        For Each fila In respuesta
            Dim res As New JavaScriptSerializer
            Dim val As String = res.Serialize(fila)
            Dim valuedes As Object = res.DeserializeObject(val)
            For Each fila2 In valuedes
                Select Case (fila2.key)
                    Case "Key"
                        Select Case (fila.key)
                            Case "Clientes"
                                RecorreCadena(fila.value)
                        End Select
                    Case "CodigoCliente"
                        CodigoC = fila2.value
                    Case "NombreCompleto"
                        Nombre = fila2.value
                    Case "CodigoReferencia"
                        CodigoReferencia = fila2.value
                    Case "DireccionEMailPrincipal"
                        EmailPrincipal = fila2.value
                    Case "DireccionEMailSecundario"
                        EmailSecundario = fila2.value
                    Case "ExcluirDeMensajesDeCorreo"
                        ExcluirMensajes = fila2.value
                        If tabla.Rows.Count = 0 Then
                            tabla.Columns.Add("CodigoCliente")
                            tabla.Columns.Add("NombreCompleto")
                            tabla.Columns.Add("CodigoReferencia")
                            tabla.Columns.Add("DireccionEMailPrincipal")
                            tabla.Columns.Add("DireccionEMailSecundario")
                            tabla.Columns.Add("ExcluirDeMensajesDeCorreo")
                        End If

                        Dim Renglon As DataRow = tabla.NewRow()
                        Renglon(0) = CodigoC
                        Renglon(1) = Nombre
                        Renglon(2) = CodigoReferencia
                        Renglon(3) = EmailPrincipal
                        Renglon(4) = EmailSecundario
                        Renglon(5) = ExcluirMensajes
                        tabla.Rows.Add(Renglon)
                End Select

            Next
        Next
        Return respuesta
    End Function
    Private Function MostrarDatos()
        Dim URL As Uri = New Uri(urlconsultaemail)
        Dim JsonString As String = String.Empty
        Dim cadena As String = String.Empty

        cadena = "{}"

        Dim data = Encoding.UTF8.GetBytes(cadena)

        Dim result_post = SendRequest(URL, data, "application/json", "POST")

        Dim json As New JavaScriptSerializer
        Try

            Dim respuesta As Object = json.DeserializeObject(result_post)
            RecorreCadena(respuesta)


        Catch ex As Exception
            'Return 0
            resultado = 0
            _errorJSON = ex.Message
            'MsgBox(_errorJSON, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try

        If tabla.Rows.Count = 0 Then
            resultado = 2
            ' Return 2
        Else
            resultado = 1
            'Return 1

        End If
        Return resultado
    End Function
    Private Function SendRequest(uri As Uri, jsonDataBytes As Byte(), contentType As String, method As String)
        Dim res As String
        Dim req As WebRequest = WebRequest.Create(uri)
        req.ContentType = contentType
        req.Headers("Authorization") = access_token
        req.Headers("GENEXUS-AGENT") = agente
        req.Method = method
        req.ContentLength = jsonDataBytes.Length

        Dim stream = req.GetRequestStream()
        stream.Write(jsonDataBytes, 0, jsonDataBytes.Length)
        stream.Close()
        Try
            Dim response = req.GetResponse().GetResponseStream()
            Dim reader As New StreamReader(response)
            res = reader.ReadToEnd()
            reader.Close()
            response.Close()
            Return res
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
            s4.Write("Titulo: " & "Error de Servidor" & vbCrLf)
            s4.Write("Mensaje: " & ex.Message & vbCrLf)
            s4.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s4.Write("=====================================================" & vbCrLf)
            s4.Close()
            fs4.Close()
            End
        End Try
    End Function

    Private Sub BotonBuscarCampos()
        MostrarDatos()
        If resultado = 0 Then
            'MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")
            s4.Write("Titulo: " & "Error" & vbCrLf)
            s4.Write("Mensaje: " & _errorJSON & vbCrLf)
            s4.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s4.Write("=====================================================" & vbCrLf)
            s4.Close()
            fs4.Close()
            End
        ElseIf resultado = 1 Then
            DtgCamposExactus.DataSource = tabla.Copy
            mensaje = DtgCamposExactus.Rows.Count & " Datos Obtenidos "

            s4.Write("Titulo: " & "Busqueda Exitosa" & vbCrLf)
            s4.Write("Mensaje: " & mensaje & vbCrLf)
            s4.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s4.Write("=====================================================" & vbCrLf)



        ElseIf resultado = 2 Then
            'MsgBox("No existen datos", MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
            mensaje = "No existen datos"
            s4.Write("Titulo: " & "Error" & vbCrLf)
            s4.Write("Mensaje: " & mensaje & vbCrLf)
            s4.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s4.Write("=====================================================" & vbCrLf)
            s4.Close()
            fs4.Close()
            FIN = 1


        End If
    End Sub


    Private Function PasarEmail()
        Try
            conexion.EjecutarNonQuery("DELETE FROM dbo.UNI_SIFCO_EMAIL ;")
            For Each fila As DataGridViewRow In DtgCamposExactus.Rows

                conexion.EjecutarNonQuery("INSERT INTO dbo.UNI_SIFCO_EMAIL (CODIGO_CLIENTE,NOMBRE_COMPLETO,CODIGO_REFERENCIA,EMAIL_PRINCIPAL,EMAIL_SECUNDARIA,EXCLUIR_MENSAJES) values ('" & fila.Cells(0).Value & "','" & fila.Cells(1).Value & "','" & fila.Cells(2).Value & "','" & fila.Cells(3).Value & "','" & fila.Cells(4).Value & "','" & fila.Cells(5).Value & "') SELECT * FROM dbo.UNI_SIFCO_EMAIL ORDER BY CODIGO_CLIENTE ")
            Next

        Catch ex As Exception
            _errorJSON = ex.Message
            ' Return 0
            resultado2 = 0
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try
        'BLOQUE TRY CATCH PARA LA INSERCCION Y ACTUALIZACION DE LA TABLA A EXACTUS
        Try

            conexion.EjecutarNonQuery("EXEC UNI_SIFCO_ACTUALIZA_EMAIL;")
            'Return 1
            resultado2 = 1


        Catch ex As Exception
            _errorJSON2 = ex.Message
            'Return 2
            resultado2 = 2


        End Try
        Return resultado2
    End Function

    Private Sub BotonPasarCampo()
        PasarEmail()

        If resultado2 = 0 Then
            ' MsgBox(_errorJSON2, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")
            s4.Write("Titulo: " & "Error" & vbCrLf)
            s4.Write("Mensaje: " & _errorJSON & vbCrLf)
            s4.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s4.Write("=====================================================" & vbCrLf)
            s4.Close()
            fs4.Close()
            End
        ElseIf resultado2 = 1 Then

            mensaje = "Datos procesados correctamente"
            s4.Write("Titulo: " & "Transferencia Exitosa" & vbCrLf)
            s4.Write("Mensaje: " & mensaje & vbCrLf)
            s4.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s4.Write("=====================================================" & vbCrLf)
            s4.Close()
            fs4.Close()

        ElseIf resultado2 = 2 Then
            '  MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")

            s4.Write("Titulo: " & "Error" & vbCrLf)
            s4.Write("Mensaje: " & _errorJSON2 & vbCrLf)
            s4.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s4.Write("=====================================================" & vbCrLf)
            s4.Close()
            fs4.Close()
            End

        End If

    End Sub



    Private Sub ConsultaCorreo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label3.Text = "Versión " & strVersion
        '*****************************************************Conexión a BD**********************************************************
        Conexion_Base()
        '****************************************************************************************************************************

        '****************************************************Consultar de Sifco******************************************************
        BotonBuscarCampos()
        '****************************************************************************************************************************

        '********************************************************Hacer Cambios ******************************************************
        If FIN = 1 Then
            End

        Else
            BotonPasarCampo()
            End
        End If

        '****************************************************************************************************************************


    End Sub
    Dim passwordt As String
    Private Sub Conexion_Base()

        Try
            conexion.BuscarConexionEnXml("cnSQL.xml", Application.StartupPath)
            ClientId = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'ClientId'")
            ClientSecret = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'ClientSecret'")
            Grant_Type = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'Grant_Type'")
            Scope = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'Scope'")
            Username = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'Username'")
            passwordt = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'Password'")
            Password = Crypto.Crypto.DesEncriptar(passwordt)
            urlautenticacion = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlAutenticacion'")
            urlcarga = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlCarga'")
            urldescargadescuentos = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlDescargaDescuento'")
            urlcargaplanilla = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlCargaPlanilla'")
            urlcambioestado = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlCambioEstado'")
            urlconsultaemail = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlConsultaEmail'")
            CMAFormatoID = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'CMAFormatoID'")
            CMArchivoTC = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'CMArchivoTC'")
            FormaIdentificar = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'FormaIdentificar'")
            OperacionAct = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'OperacionAct'")
            OperacionInac = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'OperacionInac'")
            agente = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'GENEXUS-AGENT'")
            autenticacion()

        Catch ex As Exception
            'Si no hay informacion en la tabla de la autenticacion, manda un mensaje
            'MsgBox(ex.Message, MsgBoxStyle.Critical, "SIFCO - SOFTLAND")
            s4.Write("Titulo: " & "Error Conexion" & vbCrLf)
            s4.Write("Mensaje: " & ex.Message & vbCrLf)
            s4.Write("Día/Hora: " & DateTime.Now.ToString() & vbCrLf)
            s4.Write("=====================================================" & vbCrLf)
            s4.Close()
            fs4.Close()
            End
        End Try
    End Sub
End Class
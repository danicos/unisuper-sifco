﻿Imports System.Net
Imports System.IO
Imports System.Text
Imports CARGA_PLANILLA.DTOs
Imports Newtonsoft.Json

Public Class CargaPlanillas
    Private log As Log
    Private Sub CargaPlanillas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            log = New Log()
            DBOperations.CargarXML()
            '**************Conexion a Base de datos
            Conexion_Base()
            '**************Cargar datos de softland a tablas temporales
            DBOperations.executeExtrae()
            '**************Obtener datos de Softland
            Dim planillas = DBOperations.selectPlanillas()
            '**************Aplicar descuentos dependiendo la prioridad de los productos para los descuentos aprobados en softland
            ProcesarDescuentos(planillas)
            '**************Envio de datos via rest a sifco
            EnviarDescuentos(planillas)

            log.Close()
        Catch ex As Exception
            log.WriteLog("Error", ex)
            log.Close()
        End Try
    End Sub

    Private Sub Conexion_Base()
        Try
            ClientId = DBOperations.selectParametro("ClientId")
            ClientSecret = DBOperations.selectParametro("ClientSecret")
            Grant_Type = DBOperations.selectParametro("Grant_Type")
            Scope = DBOperations.selectParametro("Scope")
            Username = DBOperations.selectParametro("Username")
            Password = Crypto.Crypto.DesEncriptar(DBOperations.selectParametro("Password"))
            urlautenticacion = DBOperations.selectParametro("UrlAutenticacion")
            urlcarga = DBOperations.selectParametro("UrlCarga")
            urldescargadescuentos = DBOperations.selectParametro("UrlDescargaDescuento")
            urlcargaplanilla = DBOperations.selectParametro("UrlCargaPlanilla")
            urlcambioestado = DBOperations.selectParametro("UrlCambioEstado")
            urlconsultaemail = DBOperations.selectParametro("UrlConsultaEmail")
            CMAFormatoID = DBOperations.selectParametro("CMAFormatoID")
            CMArchivoTC = DBOperations.selectParametro("CMArchivoTC")
            FormaIdentificar = DBOperations.selectParametro("FormaIdentificar")
            OperacionAct = DBOperations.selectParametro("OperacionAct")
            OperacionInac = DBOperations.selectParametro("OperacionInac")
            agente = DBOperations.selectParametro("GENEXUS-AGENT")
            autenticacion()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ProcesarDescuentos(planillas As List(Of Planilla))
        Try
            For Each planilla As Planilla In planillas
                planilla.Detalles = New List(Of Descuento)()
                Dim productos = DBOperations.selectProductos(planilla)
                Dim aprobados = DBOperations.selectAprobados(planilla)
                For Each aprobado As Descuento In aprobados
                    For Each producto As Producto In productos
                        Dim descuento As Descuento = DBOperations.selectDescuento(planilla, aprobado, producto)
                        If Not descuento Is Nothing Then
                            If Double.Parse(descuento.Valor) <= Double.Parse(aprobado.Valor) Then
                                aprobado.Valor = (Double.Parse(aprobado.Valor) - Double.Parse(descuento.Valor)).ToString()
                                DBOperations.updateMontoAprobadoDescuento(descuento)
                            End If
                        End If
                    Next
                    If Double.Parse(aprobado.Valor) > 0 Then
                        DBOperations.insertDescuentoNoAplicado(planilla, aprobado)
                    End If
                Next
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub EnviarDescuentos(planillas As List(Of Planilla))
        Try
            For Each planilla As Planilla In planillas
                planilla.Detalles = DBOperations.selectDescuentos(planilla)
            Next
            Dim jsonobject = New JsonDescarga()
            jsonobject.Planillas = New JsonPlanilla()
            jsonobject.Planillas.Planillas = planillas
            Dim jsontxt = JsonConvert.SerializeObject(jsonobject)
            Dim dataplanilla = Encoding.UTF8.GetBytes(jsontxt)
            Dim result_post = SendRequest(New Uri(urlcargaplanilla), dataplanilla, "application/json", "POST")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function SendRequest(uri As Uri, jsonDataBytes As Byte(), contentType As String, method As String)
        Try
            Dim res As String
            Dim req As WebRequest = WebRequest.Create(uri)
            req.ContentType = contentType
            req.Headers("Authorization") = access_token
            req.Headers("GENEXUS-AGENT") = agente
            req.Method = method
            req.ContentLength = jsonDataBytes.Length

            Dim stream = req.GetRequestStream()
            stream.Write(jsonDataBytes, 0, jsonDataBytes.Length)
            stream.Close()

            Dim response = req.GetResponse().GetResponseStream()
            Dim reader As New StreamReader(response)
            res = reader.ReadToEnd()
            reader.Close()
            response.Close()
            Return res
        Catch ex As Exception
            Throw New Exception("Error del servidor", ex)
        End Try
    End Function

End Class
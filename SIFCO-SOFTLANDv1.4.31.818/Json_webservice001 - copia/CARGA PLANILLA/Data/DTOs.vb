﻿Public Class DTOs
    Public Class JsonDescarga
        Public Property Planillas As JsonPlanilla
    End Class

    Public Class JsonPlanilla
        Public Property Planillas As List(Of Planilla)
    End Class

    Public Class Planilla
        Public Property ID As String
        Public Property Descripcion As String
        Public Property Fecha As String
        Public Property FrecuenciaCodigo As String
        Public Property FrecuenciaNombre As String
        Public Property Detalles As List(Of Descuento)
    End Class

    Public Class Producto
        Public Property Nombre As String
        Public Property Frecuencia As String
        Public Property Prioridad As String
    End Class

    Public Class Descuento
        Public Property ID As String
        Public Property ClienteCodigoSifco As String
        Public Property ClienteReferencia As String
        Public Property TransaccionNumero As String
        Public Property TransaccionReferencia As String
        Public Property Valor As String
    End Class

End Class

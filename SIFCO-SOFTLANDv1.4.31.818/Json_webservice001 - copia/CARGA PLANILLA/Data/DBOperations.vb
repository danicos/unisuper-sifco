﻿Imports System.Reflection
Imports CARGA_PLANILLA.DTOs

Public Module DBOperations

    Public Sub CargarXML()
        conexion.BuscarConexionEnXml(Constants._NAME_XMLFILE_DB, Application.StartupPath)
    End Sub
    Public Function selectParametro(name As String) As String
        Try
            Return conexion.EjecutarEscalar(String.Format(DBQuery._Q_SELECT_PARAMETRO, name))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function selectPlanillas() As List(Of Planilla)
        Try
            Dim ds = conexion.llenarDataSet(DBQuery._Q_SELECT_PLANILLAS)
            Dim res As List(Of Planilla) = ConvertDataTable(Of Planilla)(ds.Tables(0))
            Return res
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function selectAprobados(planilla As Planilla) As List(Of Descuento)
        Try
            Dim ds = conexion.llenarDataSet(String.Format(DBQuery._Q_SELECT_APROBADOS, planilla.FrecuenciaNombre))
            Dim res As List(Of Descuento) = ConvertDataTable(Of Descuento)(ds.Tables(0))
            Return res
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function selectProductos(planilla As Planilla) As List(Of Producto)
        Try
            Dim ds = conexion.llenarDataSet(String.Format(DBQuery._Q_SELECT_PRODUCTOS, planilla.FrecuenciaNombre))
            Dim res As List(Of Producto) = ConvertDataTable(Of Producto)(ds.Tables(0))
            Return res
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function selectDescuento(planilla As Planilla, aprobado As Descuento, producto As Producto) As Descuento
        Try
            Dim ds = conexion.llenarDataSet(String.Format(DBQuery._Q_SELECT_DESCUENTO_BY, aprobado.ClienteReferencia, planilla.FrecuenciaNombre, planilla.ID, producto.Nombre))
            Dim res As List(Of Descuento) = ConvertDataTable(Of Descuento)(ds.Tables(0))
            Return If(res.Count > 0, res(0), Nothing)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function insertDescuentoNoAplicado(planilla As Planilla, descuento As Descuento)
        Try
            conexion.EjecutarNonQuery(String.Format(DBQuery._Q_INSERT_DESCUENTONOAPLICADO, descuento.ClienteReferencia, descuento.TransaccionReferencia, descuento.TransaccionNumero, planilla.FrecuenciaNombre, descuento.Valor))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function updateMontoAprobadoDescuento(descuento As Descuento)
        Try
            conexion.EjecutarNonQuery(String.Format(DBQuery._Q_UPDATE_MONTOAPROBADODESCUENTO, descuento.Valor, descuento.ID))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function selectDescuentos(planilla As Planilla)
        Try
            Dim ds = conexion.llenarDataSet(String.Format(DBQuery._Q_SELECT_DESCUENTOS, planilla.ID))
            Dim res As List(Of Descuento) = ConvertDataTable(Of Descuento)(ds.Tables(0))
            Return res
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function executeExtrae()
        Try
            conexion.EjecutarStoredProc(DBQuery._Q_EXEC_EXTRAE)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function ConvertDataTable(Of T)(dt As DataTable) As List(Of T)
        Dim data As List(Of T) = New List(Of T)()
        For Each row As DataRow In dt.Rows
            Dim item As T = GetItem(Of T)(row)
            data.Add(item)
        Next
        Return data
    End Function
    Private Function GetItem(Of T)(dr As DataRow) As T
        Dim temp As Type = GetType(T)
        Dim obj As T = Activator.CreateInstance(Of T)()
        For Each column As DataColumn In dr.Table.Columns
            For Each pro As PropertyInfo In temp.GetProperties()
                If pro.Name = column.ColumnName Then
                    pro.SetValue(obj, dr(column.ColumnName).ToString(), Nothing)
                Else
                    Continue For
                End If
            Next
        Next
        Return obj
    End Function

End Module

Public Class DBQuery
    Public Const _Q_SELECT_PARAMETRO As String = "SELECT VALOR FROM dbo.UNI_SIFCO_PARAMETROS WHERE PARAMETRO = '{0}'"
    Public Const _Q_SELECT_PLANILLAS As String = "SELECT DISTINCT PLANILLA AS ID, DESCRIPCION AS Descripcion, FECHA as Fecha, FRECUENCIACODIGO AS FrecuenciaCodigo, FRECUENCIANOMBRE AS FrecuenciaNombre FROM dbo.UNI_SIFCO_DATOS_PLANILLA_WEBSERVICE"
    Public Const _Q_SELECT_APROBADOS As String = "SELECT EMPLEADO AS ClienteReferencia, MONTO AS Valor, PLANILLA AS TransaccionReferencia, DESCRIPCION AS TransaccionNumero FROM dbo.UNI_SIFCO_DATOS_PLANILLA_WEBSERVICETMP WHERE MONTO > 0 AND CONCEPTO = '{0}'"
    Public Const _Q_SELECT_PRODUCTOS As String = "SELECT NOMBRE AS Nombre, FRECUENCIA AS Frecuencia, PRIORIDAD AS Prioridad FROM UNI_SIFCO_PRODUCTO WHERE Frecuencia = '{0}' ORDER BY Prioridad ASC"
    Public Const _Q_SELECT_DESCUENTO_BY As String = "SELECT ID, CLIENTECODIGOSIFCO AS ClienteCodigoSifco, EMPLEADO AS ClienteReferencia, TRANSACCIONNUMERO AS TransaccionNumero, TRANSACCIONREFERENCIA AS TransaccionReferencia, MONTO as Valor FROM UNI_SIFCO_DATOS_PLANILLA_WEBSERVICE WHERE EMPLEADO = '{0}' AND CONCEPTO = '{1}' AND PLANILLA = '{2}' AND TRANSACCIONREFERENCIA = '{3}'"
    Public Const _Q_INSERT_DESCUENTONOAPLICADO As String = "INSERT INTO dbo.UNI_SIFCO_DESCUENTO_NO_APLICADO (EMPLEADO, NOMINA, NUMERO_NOMINA, CONCEPTO, MONTO) VALUES ('{0}','{1}','{2}','{3}', '{4}');"
    Public Const _Q_UPDATE_MONTOAPROBADODESCUENTO As String = "UPDATE dbo.UNI_SIFCO_DATOS_PLANILLA_WEBSERVICE SET MONTOAPROBADO = '{0}' WHERE ID = '{1}'"
    Public Const _Q_SELECT_DESCUENTOS As String = "SELECT ID, CLIENTECODIGOSIFCO AS ClienteCodigoSifco, EMPLEADO AS ClienteReferencia, TRANSACCIONNUMERO AS TransaccionNumero, TRANSACCIONREFERENCIA AS TransaccionReferencia, CASE WHEN MONTOAPROBADO IS NULL THEN '0.00' ELSE MONTOAPROBADO END AS Valor FROM UNI_SIFCO_DATOS_PLANILLA_WEBSERVICE WHERE PLANILLA = '{0}'"
    Public Const _Q_EXEC_EXTRAE As String = "UNI_SIFCO_PLANILLA_WB_EXTRAE"
End Class
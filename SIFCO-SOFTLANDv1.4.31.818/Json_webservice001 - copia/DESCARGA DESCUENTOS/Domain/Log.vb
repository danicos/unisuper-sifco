﻿Imports System.IO

Public Class Log
    Private fs As FileStream
    Private s As StreamWriter
    Private _STR As String = "Titulo: {0}" & vbCrLf & "Día/Hora: {1}" & vbCrLf & "Stack Trace:" & vbCrLf & "{2}" & vbCrLf & "======================================" & vbCrLf
    Public Sub New()
        fs = New FileStream(Application.StartupPath & "\LogDescarga.txt", FileMode.Append, FileAccess.Write)
        s = New StreamWriter(fs)
    End Sub
    Public Sub WriteLog(title As String, ex As Exception)
        Try
            Dim stacktrace = ""
            While Not ex Is Nothing
                stacktrace += "--" & ex.Message & vbCrLf & ex.StackTrace & vbCrLf
                ex = ex.InnerException
            End While
            s.Write(String.Format(_STR, title, DateTime.Now.ToString(), stacktrace))
        Catch e As Exception
        End Try
    End Sub
    Public Sub Close()
        s.Close()
        fs.Close()
    End Sub

End Class

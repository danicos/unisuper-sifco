﻿Public Class DTOs
    Public Class JsonDescarga
        Public Property Planillas As JsonPlanilla
    End Class

    Public Class JsonPlanilla
        Public Property Planillas As List(Of Planilla)
    End Class

    Public Class Planilla
        Public Property ID As String
        Public Property Descripcion As String
        Public Property Fecha As String
        Public Property FrecuenciaCodigo As String
        Public Property FrecuenciaNombre As String
        Public Property Detalles As List(Of Descuento)
    End Class

    Public Class Descuento
        Public ID As String
        Public ClienteCodigoSifco As String
        Public ClienteReferencia As String
        Public TransaccionNumero As String
        Public TransaccionReferencia As String
        Public Valor As String
    End Class

End Class

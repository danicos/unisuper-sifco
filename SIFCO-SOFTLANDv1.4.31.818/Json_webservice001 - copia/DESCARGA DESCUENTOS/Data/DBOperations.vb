﻿Imports System.Data.SqlClient
Imports DESCARGA_DESCUENTOS.DTOs

Public Module DBOperations

    Public Sub CargarXML()
        conexion.BuscarConexionEnXml(Constants._NAME_XMLFILE_DB, Application.StartupPath)
    End Sub
    Public Function selectParametro(name As String) As String
        Try
            Return conexion.EjecutarEscalar(String.Format(DBQuery._Q_SELECT_PARAMETRO, name))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub deletePlanillaWebService()
        Try
            conexion.EjecutarNonQuery(DBQuery._Q_DELETE_PLANILLAWEBSERVICE)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub insertPlanillaWebService(planilla As Planilla, descuento As Descuento, inicio As String, final As String)
        Try
            conexion.EjecutarNonQuery(String.Format(DBQuery._Q_INSERT_PLANILLAWEBSERVICE,
                                                    descuento.ClienteReferencia,
                                                    planilla.FrecuenciaNombre,
                                                    inicio,
                                                    final,
                                                    descuento.Valor,
                                                    planilla.ID,
                                                    planilla.Descripcion,
                                                    planilla.Fecha,
                                                    planilla.FrecuenciaCodigo,
                                                    planilla.FrecuenciaNombre,
                                                    descuento.ID,
                                                    descuento.ClienteCodigoSifco,
                                                    descuento.TransaccionNumero,
                                                    descuento.TransaccionReferencia.Trim()
                                    ))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub execPlanilla()
        Try
            conexion.EjecutarStoredProc(DBQuery._Q_EXEC_PLANILLA)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Module

Public Class DBQuery
    Public Const _Q_SELECT_PARAMETRO As String = "SELECT VALOR FROM dbo.UNI_SIFCO_PARAMETROS WHERE PARAMETRO = '{0}'"
    Public Const _Q_DELETE_PLANILLAWEBSERVICE As String = "DELETE FROM dbo.UNI_SIFCO_DATOS_PLANILLA_WEBSERVICE;"
    Public Const _Q_INSERT_PLANILLAWEBSERVICE As String = "INSERT INTO dbo.UNI_SIFCO_DATOS_PLANILLA_WEBSERVICE (EMPLEADO,CONCEPTO,FECHA_INICIAL,FECHA_FINAL,MONTO,PLANILLA,DESCRIPCION,FECHA,FRECUENCIACODIGO,FRECUENCIANOMBRE,ID,CLIENTECODIGOSIFCO,TRANSACCIONNUMERO, TRANSACCIONREFERENCIA) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}')"
    Public Const _Q_EXEC_PLANILLA As String = "UNI_SIFCO_PLANILLA_WB_INSERTA"
End Class
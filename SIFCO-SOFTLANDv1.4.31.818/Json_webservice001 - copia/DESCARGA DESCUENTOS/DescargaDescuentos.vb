﻿Imports System.Net
Imports System.IO
Imports System.Text
Imports DESCARGA_DESCUENTOS.DTOs
Imports Newtonsoft.Json

Public Class DescargaDescuentos

    Private log As Log

    Private Sub DescargaDescuentos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            log = New Log()
            DBOperations.CargarXML()
            '**************Conexion a Base de datos
            Conexion_Base()
            '**************Rango de Fecha
            CalcularFecha()
            '**************Descargar Planilla de Sifco
            Dim planillas As List(Of Planilla) = DescargarPlanilla()
            '**************Cargar datos a Softland
            If Not planillas Is Nothing Then
                CargarPlanilla(planillas)
            End If
            log.Close()
        Catch ex As Exception
            log.WriteLog("Error", ex)
            log.Close()
        End Try
    End Sub

    Private Sub Conexion_Base()
        Try
            ClientId = DBOperations.selectParametro("ClientId")
            ClientSecret = DBOperations.selectParametro("ClientSecret")
            Grant_Type = DBOperations.selectParametro("Grant_Type")
            Scope = DBOperations.selectParametro("Scope")
            Username = DBOperations.selectParametro("Username")
            Password = Crypto.Crypto.DesEncriptar(DBOperations.selectParametro("Password"))
            urlautenticacion = DBOperations.selectParametro("UrlAutenticacion")
            urlcarga = DBOperations.selectParametro("UrlCarga")
            urldescargadescuentos = DBOperations.selectParametro("UrlDescargaDescuento")
            urlcargaplanilla = DBOperations.selectParametro("UrlCargaPlanilla")
            urlcambioestado = DBOperations.selectParametro("UrlCambioEstado")
            urlconsultaemail = DBOperations.selectParametro("UrlConsultaEmail")
            CMAFormatoID = DBOperations.selectParametro("CMAFormatoID")
            CMArchivoTC = DBOperations.selectParametro("CMArchivoTC")
            FormaIdentificar = DBOperations.selectParametro("FormaIdentificar")
            OperacionAct = DBOperations.selectParametro("OperacionAct")
            OperacionInac = DBOperations.selectParametro("OperacionInac")
            agente = DBOperations.selectParametro("GENEXUS-AGENT")
            autenticacion()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CalcularFecha()
        Dim Fechaprueba As Date = #06/03/2019 #
        FechaDesde = Format(Fechaprueba, "MM/dd/yyyy")
        FechaHasta = Format(Fechaprueba, "MM/dd/yyyy")
    End Sub

    Private Function DescargarPlanilla() As List(Of Planilla)
        Try
            Dim URI As Uri = New Uri(urldescargadescuentos)
            Dim jsonString As String = String.Format(Constants._JSON_REQ_DESCARGA, "{", FechaDesde, FechaHasta, "}")
            Dim data = Encoding.UTF8.GetBytes(jsonString)
            Dim result_post = SendRequestDescarga(URI, data, "application/json", "POST")
            Dim planillas As JsonDescarga = JsonConvert.DeserializeObject(Of JsonDescarga)(result_post)
            Return planillas.Planillas.Planillas
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub CargarPlanilla(planillas As List(Of Planilla))
        Try
            DBOperations.deletePlanillaWebService()
            For Each planilla As Planilla In planillas
                For Each descuento As Descuento In planilla.Detalles
                    DBOperations.insertPlanillaWebService(planilla, descuento, FechaDesde, FechaHasta)
                Next
            Next
            DBOperations.execPlanilla()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function SendRequestDescarga(uri As Uri, jsonDataBytes As Byte(), contentType As String, method As String)
        Try
            Dim res As String
            Dim req As WebRequest = WebRequest.Create(uri)
            req.ContentType = contentType
            req.Headers("Authorization") = access_token
            req.Headers("GENEXUS-AGENT") = agente
            req.Method = method
            req.ContentLength = jsonDataBytes.Length

            Dim stream = req.GetRequestStream()
            stream.Write(jsonDataBytes, 0, jsonDataBytes.Length)
            stream.Close()

            Dim response = req.GetResponse().GetResponseStream()
            Dim reader As New StreamReader(response)
            res = reader.ReadToEnd()
            reader.Close()
            response.Close()
            Return res
        Catch ex As Exception
            Throw New Exception("Error en el Servidor", ex)
        End Try
    End Function

End Class

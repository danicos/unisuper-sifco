﻿Imports System
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Web.Script.Serialization
Imports System.Threading

Public Class CargaMasiva
    Dim contador As Integer
    Dim tabla As DataSet

    Public Async Function CargaEmpleados() As Task(Of Integer)

        Try
            Dim URI As Uri = New Uri(urlcarga)
            Dim jsonString As String = String.Empty


            Dim encabezadoJson As String = "{
          ""CargasMasivasAutomaticasFormatoID"": " & CMAFormatoID & ",
          ""CargasMasivasArchivoTipoCarga"": " & CMArchivoTC & ",
          ""CargasMasivasWSCampos_SDT"": [ " & vbCrLf
            Dim encabezadoFila As String = "{
              ""PersonaCampos"": [ " & vbCrLf

            Dim finFila As String = "      ]
            } "
            Dim finJson As String = vbCrLf & "          ]
        }"
            Dim valores As String = String.Empty
            Dim icontador As Integer = 0

            'Pantalla de espera del la respuesta

            ' PEspera1.Visible = True

            'Await Task.Delay(2000)
            If dtgDatos.Rows.Count > 0 Then
                'Recorremos fila por fila para obtener los valores
                For Each fila As DataGridViewRow In dtgDatos.Rows
                    'le damos el encabezado a la fila.

                    If icontador > 0 Then
                        valores &= ", " & vbCrLf
                    End If

                    valores &= encabezadoFila

                    'recorremos columna por columna por cada fila
                    For i As Integer = 0 To dtgDatos.Columns.Count - 1
                        'se incluye una coma despues de la primer columna...
                        If i > 0 Then
                            valores &= ", " & vbCrLf
                        End If
                        valores = valores & "{""Valor"": """ & fila.Cells(i).Value.ToString.Replace("""", String.Empty).Replace(vbCr, String.Empty) & """}"

                    Next

                    valores &= finFila

                    'aumentamos contador...
                    icontador += 1
                Next

                jsonString = encabezadoJson & valores & finJson
                Dim data = Encoding.UTF8.GetBytes(jsonString)


                'Dim result_post = Await 
                SendRequest(URI, data, "application/json", "POST")
                'MsgBox(result_post, MsgBoxStyle.Information, "SIFCO - SOFTLAND")
                Return 1
            Else
                'MsgBox("No hay datos a enviar", MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
                Return 2
            End If

        Catch ex As Exception
            _errorJSON = ex.Message
            Return 0
        End Try


    End Function

    Private _errorJSON As String

    Private Sub SendRequest(uri As Uri, jsonDataBytes As Byte(), contentType As String, method As String) 'As Task(Of String)

        Dim req As WebRequest = WebRequest.Create(uri)
        'Cargando.ShowDialog()
        req.ContentType = contentType
        req.Headers("Authorization") = access_token
        req.Headers("GENEXUS-AGENT") = agente
        req.Method = method
        req.ContentLength = jsonDataBytes.Length

        Try
            Dim stream = req.GetRequestStream()
            stream.Write(jsonDataBytes, 0, jsonDataBytes.Length)
            stream.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try
        Dim res As String
        Dim response = req.GetResponse().GetResponseStream()
        Dim reader As New StreamReader(response)
        res = reader.ReadToEnd()
        reader.Close()
        response.Close()
        'Await Task.Delay(2000)
        'Return res

    End Sub

    Private Sub CargaMasiva_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Label3.Text = "Versión " & strVersion
    End Sub
    Private Async Function Descarga() As Task(Of Integer)

        'Try.. catch para la carga en el datagridview
        Try

            tabla = conexion.llenarDataSet("Exec UNI_SIFCO_EMPLEADOS;")

            If tabla.Tables(0).Rows.Count = 0 Then
                Return 2
            Else

                Return 1
            End If

        Catch ex As Exception
            _errorJSON = ex.Message
            Return 0
            ' MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try
    End Function

    Private Async Sub TslCargaDB_Click(sender As Object, e As EventArgs) Handles tslCargaDB.Click
        PEspera1.Visible = True
        tslCargaDB.Enabled = False
        tslCargaWS.Enabled = False
        btnCerrar.Enabled = False

        Application.DoEvents()

        Dim resultado As Integer = Await Task.Run(Function()
                                                      Return Descarga()
                                                  End Function)

        If resultado = 0 Then
            MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")
        ElseIf resultado = 1 Then
            dtgDatos.DataSource = tabla.Tables(0)
            tslCargaDB.Enabled = False
            tslLimpiarTabla.Visible = True
            tslCargaWS.Enabled = True
            MsgBox(dtgDatos.Rows.Count & " Colaboradores cargados. ", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
        ElseIf resultado = 2 Then
            dtgDatos.DataSource = ""
            tslCargaWS.Enabled = False
            tslCargaDB.Enabled = True
            tslLimpiarTabla.Visible = False
            MsgBox("No hay datos nuevos", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
        End If


        PEspera1.Visible = False
        'tslCargaDB.Enabled = False
        'tslLimpiarTabla.Visible = True
        'tslCargaWS.Enabled = True
        btnCerrar.Enabled = True



    End Sub


    Async Sub TslCargaWS_Click(sender As Object, e As EventArgs) Handles tslCargaWS.Click



        PEspera1.Visible = True
        tslCargaDB.Enabled = False
        tslCargaWS.Enabled = False
        btnCerrar.Enabled = False

        Application.DoEvents()

        'Se llama a la Funcion Carga empleados

        Dim resultado As Integer = Await Task.Run(Function()
                                                      Return CargaEmpleados()
                                                  End Function)

        If resultado = 0 Then
            MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")
        ElseIf resultado = 1 Then
            MsgBox(dtgDatos.Rows.Count & " Colaboradores cargados. ", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
            tslCargaDB.Enabled = False
            tslLimpiarTabla.Visible = True
            tslCargaWS.Enabled = False
        ElseIf resultado = 2 Then
            MsgBox("No hay datos a enviar", MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
            tslCargaDB.Enabled = True
            tslLimpiarTabla.Visible = False
            tslCargaWS.Enabled = False
        End If

        PEspera1.Visible = False

        btnCerrar.Enabled = True




    End Sub





    Private Sub TslLimpiarTabla_Click(sender As Object, e As EventArgs) Handles tslLimpiarTabla.Click

        'Se inicia el data grid view
        dtgDatos.DataSource = ""

        'Se hace no visible al boton Limpiar Tabla
        tslLimpiarTabla.Visible = False
        tslCargaWS.Enabled = False
        tslCargaDB.Enabled = True
        tabla.Reset()

    End Sub


    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click

        'Mensaje para regresar al menu
        If (MessageBox.Show("¿Desea regresar al menú?", "SIFCO - SOFTLAND",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            Me.Close()
            Menu_Carga.Show()
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub
End Class

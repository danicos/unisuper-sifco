﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DescargaDescuentos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DescargaDescuentos))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.tlsRangoFecha = New System.Windows.Forms.ToolStripButton()
        Me.tlsDescargaPlanilla = New System.Windows.Forms.ToolStripButton()
        Me.tlsCargarBase = New System.Windows.Forms.ToolStripButton()
        Me.tlsLimpiarEditar = New System.Windows.Forms.ToolStripButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.BtnSalir = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtgListadoPlanilla = New System.Windows.Forms.DataGridView()
        Me.tsbDescargaPlanilla = New System.Windows.Forms.ToolStripButton()
        Me.tsbDescargarPlanilla = New System.Windows.Forms.ToolStripButton()
        Me.dtgListadoCargos = New System.Windows.Forms.DataGridView()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtgNoExisten = New System.Windows.Forms.DataGridView()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.PEspera5 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ToolStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dtgListadoPlanilla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtgListadoCargos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtgNoExisten, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PEspera5.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(88, Byte), Integer))
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.ToolStripSeparator1, Me.tlsRangoFecha, Me.tlsDescargaPlanilla, Me.tlsCargarBase, Me.tlsLimpiarEditar})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1329, 55)
        Me.ToolStrip1.TabIndex = 111
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripLabel1.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(365, 52)
        Me.ToolStripLabel1.Text = "Descuentos a colaboradores SOFTLAND"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 55)
        '
        'tlsRangoFecha
        '
        Me.tlsRangoFecha.AutoSize = False
        Me.tlsRangoFecha.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlsRangoFecha.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.tlsRangoFecha.Image = CType(resources.GetObject("tlsRangoFecha.Image"), System.Drawing.Image)
        Me.tlsRangoFecha.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tlsRangoFecha.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlsRangoFecha.Name = "tlsRangoFecha"
        Me.tlsRangoFecha.Size = New System.Drawing.Size(180, 52)
        Me.tlsRangoFecha.Text = "Rango de Fechas"
        Me.tlsRangoFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tlsDescargaPlanilla
        '
        Me.tlsDescargaPlanilla.AutoSize = False
        Me.tlsDescargaPlanilla.Enabled = False
        Me.tlsDescargaPlanilla.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlsDescargaPlanilla.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.tlsDescargaPlanilla.Image = CType(resources.GetObject("tlsDescargaPlanilla.Image"), System.Drawing.Image)
        Me.tlsDescargaPlanilla.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tlsDescargaPlanilla.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlsDescargaPlanilla.Name = "tlsDescargaPlanilla"
        Me.tlsDescargaPlanilla.Size = New System.Drawing.Size(187, 52)
        Me.tlsDescargaPlanilla.Text = "Descargar De SIFCO"
        Me.tlsDescargaPlanilla.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tlsCargarBase
        '
        Me.tlsCargarBase.AutoSize = False
        Me.tlsCargarBase.Enabled = False
        Me.tlsCargarBase.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlsCargarBase.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.tlsCargarBase.Image = CType(resources.GetObject("tlsCargarBase.Image"), System.Drawing.Image)
        Me.tlsCargarBase.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tlsCargarBase.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlsCargarBase.Name = "tlsCargarBase"
        Me.tlsCargarBase.Size = New System.Drawing.Size(185, 52)
        Me.tlsCargarBase.Text = "Enviar a SOFTLAND"
        Me.tlsCargarBase.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tlsLimpiarEditar
        '
        Me.tlsLimpiarEditar.AutoSize = False
        Me.tlsLimpiarEditar.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tlsLimpiarEditar.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.tlsLimpiarEditar.Image = CType(resources.GetObject("tlsLimpiarEditar.Image"), System.Drawing.Image)
        Me.tlsLimpiarEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.tlsLimpiarEditar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tlsLimpiarEditar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlsLimpiarEditar.Name = "tlsLimpiarEditar"
        Me.tlsLimpiarEditar.Size = New System.Drawing.Size(180, 52)
        Me.tlsLimpiarEditar.Text = "Editar y Limpiar"
        Me.tlsLimpiarEditar.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.BtnSalir)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 609)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1329, 52)
        Me.Panel1.TabIndex = 112
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(13, 17)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 23)
        Me.Label8.TabIndex = 4
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BtnSalir
        '
        Me.BtnSalir.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnSalir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSalir.FlatAppearance.BorderSize = 0
        Me.BtnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSalir.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSalir.Image = CType(resources.GetObject("BtnSalir.Image"), System.Drawing.Image)
        Me.BtnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnSalir.Location = New System.Drawing.Point(1237, 9)
        Me.BtnSalir.Name = "BtnSalir"
        Me.BtnSalir.Size = New System.Drawing.Size(80, 35)
        Me.BtnSalir.TabIndex = 2
        Me.BtnSalir.Text = "Salir"
        Me.BtnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnSalir.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(175, Byte), Integer), CType(CType(175, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.Panel2.Location = New System.Drawing.Point(-4, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1345, 1)
        Me.Panel2.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(346, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 29)
        Me.Label1.TabIndex = 115
        Me.Label1.Text = "Desde"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(690, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 29)
        Me.Label2.TabIndex = 116
        Me.Label2.Text = "Hasta"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtgListadoPlanilla
        '
        Me.dtgListadoPlanilla.AllowUserToAddRows = False
        Me.dtgListadoPlanilla.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(234, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(197, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.dtgListadoPlanilla.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgListadoPlanilla.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtgListadoPlanilla.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dtgListadoPlanilla.BackgroundColor = System.Drawing.Color.White
        Me.dtgListadoPlanilla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(197, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgListadoPlanilla.DefaultCellStyle = DataGridViewCellStyle2
        Me.dtgListadoPlanilla.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dtgListadoPlanilla.Location = New System.Drawing.Point(55, 181)
        Me.dtgListadoPlanilla.Name = "dtgListadoPlanilla"
        Me.dtgListadoPlanilla.ReadOnly = True
        Me.dtgListadoPlanilla.Size = New System.Drawing.Size(379, 380)
        Me.dtgListadoPlanilla.TabIndex = 117
        Me.dtgListadoPlanilla.TabStop = False
        '
        'tsbDescargaPlanilla
        '
        Me.tsbDescargaPlanilla.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsbDescargaPlanilla.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.tsbDescargaPlanilla.Image = CType(resources.GetObject("tsbDescargaPlanilla.Image"), System.Drawing.Image)
        Me.tsbDescargaPlanilla.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.tsbDescargaPlanilla.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsbDescargaPlanilla.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbDescargaPlanilla.Name = "tsbDescargaPlanilla"
        Me.tsbDescargaPlanilla.Size = New System.Drawing.Size(174, 52)
        Me.tsbDescargaPlanilla.Text = "Descargar Planillas"
        Me.tsbDescargaPlanilla.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tsbDescargarPlanilla
        '
        Me.tsbDescargarPlanilla.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsbDescargarPlanilla.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.tsbDescargarPlanilla.Image = CType(resources.GetObject("tsbDescargarPlanilla.Image"), System.Drawing.Image)
        Me.tsbDescargarPlanilla.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.tsbDescargarPlanilla.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsbDescargarPlanilla.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbDescargarPlanilla.Name = "tsbDescargarPlanilla"
        Me.tsbDescargarPlanilla.Size = New System.Drawing.Size(174, 52)
        Me.tsbDescargarPlanilla.Text = "Descargar Planillas"
        Me.tsbDescargarPlanilla.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtgListadoCargos
        '
        Me.dtgListadoCargos.AllowUserToAddRows = False
        Me.dtgListadoCargos.AllowUserToDeleteRows = False
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(234, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(197, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        Me.dtgListadoCargos.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dtgListadoCargos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.dtgListadoCargos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dtgListadoCargos.BackgroundColor = System.Drawing.Color.White
        Me.dtgListadoCargos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(197, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgListadoCargos.DefaultCellStyle = DataGridViewCellStyle4
        Me.dtgListadoCargos.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dtgListadoCargos.Location = New System.Drawing.Point(462, 181)
        Me.dtgListadoCargos.Name = "dtgListadoCargos"
        Me.dtgListadoCargos.ReadOnly = True
        Me.dtgListadoCargos.Size = New System.Drawing.Size(805, 380)
        Me.dtgListadoCargos.TabIndex = 118
        Me.dtgListadoCargos.TabStop = False
        '
        'dtpDesde
        '
        Me.dtpDesde.AccessibleRole = System.Windows.Forms.AccessibleRole.Text
        Me.dtpDesde.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.dtpDesde.CalendarFont = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDesde.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.dtpDesde.CustomFormat = "dd/MM/yyyy"
        Me.dtpDesde.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDesde.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.dtpDesde.Location = New System.Drawing.Point(448, 89)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(125, 27)
        Me.dtpDesde.TabIndex = 119
        Me.dtpDesde.Value = New Date(2018, 5, 8, 17, 0, 54, 0)
        '
        'dtpHasta
        '
        Me.dtpHasta.AccessibleRole = System.Windows.Forms.AccessibleRole.Text
        Me.dtpHasta.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.dtpHasta.CalendarFont = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHasta.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.dtpHasta.CustomFormat = "dd/MM/yyyy"
        Me.dtpHasta.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpHasta.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.dtpHasta.Location = New System.Drawing.Point(777, 89)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(125, 27)
        Me.dtpHasta.TabIndex = 120
        Me.dtpHasta.Value = New Date(2018, 5, 8, 17, 1, 15, 0)
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(130, 142)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(204, 29)
        Me.Label3.TabIndex = 121
        Me.Label3.Text = "Listado de Planillas"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label4.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(703, 142)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(272, 29)
        Me.Label4.TabIndex = 122
        Me.Label4.Text = "Listado de Cargo por Planilla"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtgNoExisten
        '
        Me.dtgNoExisten.AllowUserToAddRows = False
        Me.dtgNoExisten.AllowUserToDeleteRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(234, Byte), Integer), CType(CType(234, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(197, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        Me.dtgNoExisten.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dtgNoExisten.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgNoExisten.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dtgNoExisten.BackgroundColor = System.Drawing.Color.White
        Me.dtgNoExisten.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(197, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgNoExisten.DefaultCellStyle = DataGridViewCellStyle6
        Me.dtgNoExisten.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dtgNoExisten.Location = New System.Drawing.Point(1106, 181)
        Me.dtgNoExisten.Name = "dtgNoExisten"
        Me.dtgNoExisten.Size = New System.Drawing.Size(161, 380)
        Me.dtgNoExisten.TabIndex = 123
        Me.dtgNoExisten.TabStop = False
        Me.dtgNoExisten.Visible = False
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(1084, 142)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(204, 29)
        Me.Label5.TabIndex = 124
        Me.Label5.Text = "Listado no existentes"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label5.Visible = False
        '
        'PEspera5
        '
        Me.PEspera5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PEspera5.BackColor = System.Drawing.Color.FromArgb(CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.PEspera5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PEspera5.Controls.Add(Me.Label6)
        Me.PEspera5.Controls.Add(Me.Panel3)
        Me.PEspera5.Controls.Add(Me.PictureBox1)
        Me.PEspera5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PEspera5.Location = New System.Drawing.Point(389, 189)
        Me.PEspera5.Name = "PEspera5"
        Me.PEspera5.Size = New System.Drawing.Size(567, 283)
        Me.PEspera5.TabIndex = 125
        Me.PEspera5.Visible = False
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Segoe UI Semibold", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(213, 75)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(135, 30)
        Me.Label6.TabIndex = 107
        Me.Label6.Text = "Cargando..."
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(88, Byte), Integer))
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(565, 46)
        Me.Panel3.TabIndex = 106
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label7.Location = New System.Drawing.Point(15, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(144, 28)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "SIFCO-SOFTLAND"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel4
        '
        Me.Panel4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(175, Byte), Integer), CType(CType(175, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.Panel4.Location = New System.Drawing.Point(-4, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(581, 1)
        Me.Panel4.TabIndex = 1
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(203, 119)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(153, 135)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'DescargaDescuentos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1329, 661)
        Me.Controls.Add(Me.PEspera5)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dtgNoExisten)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtpHasta)
        Me.Controls.Add(Me.dtpDesde)
        Me.Controls.Add(Me.dtgListadoCargos)
        Me.Controls.Add(Me.dtgListadoPlanilla)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "DescargaDescuentos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SIFCO-SOFTLAND"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dtgListadoPlanilla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtgListadoCargos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtgNoExisten, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PEspera5.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripLabel1 As ToolStripLabel
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents dtgListadoPlanilla As DataGridView
    Friend WithEvents tsbDescargaPlanilla As ToolStripButton
    Friend WithEvents tsbDescargarPlanilla As ToolStripButton
    Friend WithEvents tlsDescargaPlanilla As ToolStripButton
    Friend WithEvents tlsRangoFecha As ToolStripButton
    Friend WithEvents tlsCargarBase As ToolStripButton
    Friend WithEvents dtgListadoCargos As DataGridView
    Friend WithEvents dtpDesde As DateTimePicker
    Friend WithEvents dtpHasta As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents tlsLimpiarEditar As ToolStripButton
    Friend WithEvents dtgNoExisten As DataGridView
    Friend WithEvents Label5 As Label
    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents BindingSource2 As BindingSource
    Friend WithEvents BtnSalir As Button
    Friend WithEvents PEspera5 As Panel
    Friend WithEvents Label6 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label7 As Label
    Friend WithEvents Panel4 As Panel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label8 As Label
End Class

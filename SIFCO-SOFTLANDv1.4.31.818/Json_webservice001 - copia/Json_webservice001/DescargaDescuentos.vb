﻿Imports System
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json
Imports System.Web.Script.Services

Public Class DescargaDescuentos
    'Dim Concepto As New DataTable
    Dim tablad As DataSet
    Dim tabla As New DataTable
    Dim tabla2 As New DataTable
    Dim contadorf As Integer
    Dim valor As String
    Private _errorJSON As String
    Private Function RecibeDatos(respuesta As Object)

        For Each fila In respuesta
            Dim res As New JavaScriptSerializer
            Dim val As String = res.Serialize(fila)
            Dim valuedes As Object = res.DeserializeObject(val)
            'Key: es el nombre de la variable que estamos recuperando....

            For Each fila2 In valuedes
                Select Case (fila2.key)
                    Case "Key"
                        Select Case (fila.key)
                            Case "Planillas"
                                RecibeDatos(fila.value)
                        End Select
                    Case "ID"
                        If valor = "Detalles" And contadorf <= respuesta.length Then
                            idDeta = fila2.value
                        Else
                            id = fila2.value
                            valor = ""
                            contadorf = 0

                        End If

                    Case "Descripcion"
                        Descripcion = fila2.value
                    Case "Fecha"
                        Fecha = fila2.value
                    Case "FrecuenciaCodigo"
                        FrecuenciaCodigo = fila2.value
                    Case "FrecuenciaNombre"
                        FrecuenciaNombre = fila2.value
                        'Planilla
                        If tabla.Rows.Count = 0 Then
                            tabla.Columns.Add("ID")
                            tabla.Columns.Add("Descripcion")
                            tabla.Columns.Add("Fecha")
                            tabla.Columns.Add("FrecuenciaCodigo")
                            tabla.Columns.Add("FrecuenciaNombre")
                        End If

                        Dim Renglon As DataRow = tabla.NewRow()
                        Renglon(0) = id
                        Renglon(1) = Descripcion.TrimEnd
                        Renglon(2) = Fecha
                        Renglon(3) = FrecuenciaCodigo
                        Renglon(4) = FrecuenciaNombre
                        tabla.Rows.Add(Renglon)

                    Case "Detalles"
                        valor = fila2.key
                        RecibeDatos(fila2.value)

                    Case "ClienteCodigoSifco"
                        ClienteCodigoSifco = fila2.value

                    Case "ClienteReferencia"
                        ClienteReferencia = fila2.value

                    Case "TransaccionNumero"
                        TransaccionNumero = fila2.value

                    Case "TransaccionReferencia"
                        TransaccionReferencia = fila2.value.ToString.Replace(" ", String.Empty)
                      '  EncuentraConcepto()

                    Case "Valor"
                        Monto = fila2.value

                        'listado
                        If tabla2.Rows.Count = 0 Then
                            tabla2.Columns.Add("ID")
                            tabla2.Columns.Add("ClienteCodigoSifco")
                            tabla2.Columns.Add("ClienteReferencia")
                            tabla2.Columns.Add("TransaccionNumero")
                            tabla2.Columns.Add("TransaccionReferencia")
                            tabla2.Columns.Add("Valor")
                            tabla2.Columns.Add("Planilla")
                        End If
                        Dim renglon2 As DataRow = tabla2.NewRow()
                        renglon2(0) = idDeta
                        renglon2(1) = ClienteCodigoSifco
                        renglon2(2) = ClienteReferencia
                        renglon2(3) = TransaccionNumero
                        renglon2(4) = TransaccionReferencia
                        renglon2(5) = Monto
                        renglon2(6) = id
                        tabla2.Rows.Add(renglon2)
                        contadorf += 1

                End Select

            Next



        Next
        Return respuesta
    End Function

    Private Async Function DescargaPlanilla() As Task(Of Integer)
        Dim URI As Uri = New Uri(urldescargadescuentos)
        Dim jsonString As String = String.Empty

        Dim InicioJson As String =
       "{
          ""Desde"": """ & FechaDesde & """, 
          ""Hasta"": """ & FechaHasta & """
        }"
        jsonString = InicioJson

        Dim data = Encoding.UTF8.GetBytes(jsonString)

        Dim result_post = SendRequestDescarga(URI, data, "application/json", "POST")


        Dim json As New JavaScriptSerializer

        Try
            Dim respuesta As Object = json.DeserializeObject(result_post)
            RecibeDatos(respuesta)

        Catch ex As Exception
            _errorJSON = ex.Message
            Return 0
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try

        If tabla.Rows.Count = 0 And tabla2.Rows.Count = 0 Then
            'MsgBox("No existen datos, cambie el rango de fecha ", MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
            Return 2
            'tlsCargarBase.Enabled = False
            'tlsLimpiarEditar.Visible = False
            'tlsDescargaPlanilla.Enabled = False
            'tlsRangoFecha.Enabled = True
            'dtpDesde.Enabled = True
            'dtpHasta.Enabled = True
            'dtpDesde.ResetText()
            'dtpHasta.ResetText()
            'dtpDesde.Focus()

        Else
            'MsgBox("Datos Obtenidos ", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
            Return 1
            'dtgListadoPlanilla.DataSource = tabla.Copy
            'dtgListadoCargos.DataSource = tabla2.Copy
            'tlsCargarBase.Enabled = True
        End If
    End Function

    Private Function SendRequestDescarga(uri As Uri, jsonDataBytes As Byte(), contentType As String, method As String)
        Dim res As String
        Dim req As WebRequest = WebRequest.Create(uri)
        req.ContentType = contentType
        req.Headers("Authorization") = access_token
        req.Headers("GENEXUS-AGENT") = agente
        req.Method = method
        req.ContentLength = jsonDataBytes.Length

        Dim stream = req.GetRequestStream()
        stream.Write(jsonDataBytes, 0, jsonDataBytes.Length)
        stream.Close()
        Try
            Dim response = req.GetResponse().GetResponseStream()
            Dim reader As New StreamReader(response)
            res = reader.ReadToEnd()
            reader.Close()
            response.Close()
            Return res
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try

    End Function


    Private Sub DescargaDescuentos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Dim dtFecha As Date = DateSerial(Year(Date.Today), 1, 1)
        Me.dtpDesde.Value = Date.Today
        Me.dtpHasta.Value = Date.Today
        Label8.Text = "Versión " & strVersion
    End Sub

    Async Sub tlsDescargaPlanilla_Click(sender As Object, e As EventArgs) Handles tlsDescargaPlanilla.Click

        sender.enabled = False
        PEspera5.Visible = True
        tlsCargarBase.Enabled = False
        tlsDescargaPlanilla.Enabled = False
        tlsLimpiarEditar.Visible = False
        tlsRangoFecha.Enabled = False
        BtnSalir.Enabled = False

        Application.DoEvents()

        Dim resultado As Integer = Await Task.Run(Function()
                                                      Return DescargaPlanilla()
                                                  End Function)
        If resultado = 0 Then
            MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")
            tlsLimpiarEditar.Visible = False
        ElseIf resultado = 1 Then
            dtgListadoPlanilla.DataSource = tabla.Copy
            dtgListadoCargos.DataSource = tabla2.Copy
            MsgBox(dtgListadoCargos.Rows.Count & " Datos Obtenidos ", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
            tlsCargarBase.Enabled = True
            tlsLimpiarEditar.Visible = True
        ElseIf resultado = 2 Then
            MsgBox("No existen datos, cambie el rango de fecha ", MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
            tlsCargarBase.Enabled = False
            tlsLimpiarEditar.Visible = False
            tlsDescargaPlanilla.Enabled = False
            tlsRangoFecha.Enabled = True
            dtpDesde.Enabled = True
            dtpHasta.Enabled = True
            dtpDesde.ResetText()
            dtpHasta.ResetText()
            dtpDesde.Focus()
        End If

        PEspera5.Visible = False
        tlsDescargaPlanilla.Enabled = False
        BtnSalir.Enabled = True


    End Sub

    Private Sub tlsRangoFecha_Click(sender As Object, e As EventArgs) Handles tlsRangoFecha.Click

        tlsLimpiarEditar.Visible = True

        FechaDesde = Format(dtpDesde.Value, "MM/dd/yyyy")
        FechaHasta = Format(dtpHasta.Value, "MM/dd/yyyy")
        If dtpDesde.Value > dtpHasta.Value Then
            MsgBox("La fecha Desde no debe ser Mayor", MsgBoxStyle.Critical, "SIFCO - SOFTLAND")
            FechaDesde = ""
            FechaHasta = ""
            dtpDesde.ResetText()
            dtpDesde.Focus()
        ElseIf dtpDesde.Value < dtpHasta.Value Then

            dtpDesde.Enabled = False
            dtpHasta.Enabled = False
            tlsDescargaPlanilla.Enabled = True
            tlsRangoFecha.Enabled = False
        ElseIf dtpDesde.Value = dtpHasta.Value Then
            dtpDesde.Enabled = False
            dtpHasta.Enabled = False
            tlsDescargaPlanilla.Enabled = True
            tlsRangoFecha.Enabled = False

        End If


    End Sub

    Private Sub tlsLimpiarEditar_Click(sender As Object, e As EventArgs) Handles tlsLimpiarEditar.Click

        'LIMPIA A NIVEL DE DATEPICKER
        tlsRangoFecha.Enabled = True
        dtpDesde.Enabled = True
        dtpHasta.Enabled = True
        dtpDesde.ResetText()
        dtpHasta.ResetText()
        tlsLimpiarEditar.Visible = False
        tlsCargarBase.Enabled = False

        ' LINICIALIZA TABLAS Y DATAGRIDVIEW
        tabla.Reset()
        tabla.Clear()
        tabla2.Reset()
        tabla2.Clear()
        'tablad.Reset()
        'tablad.Clear()

        dtgListadoPlanilla.DataSource = ""
        dtgListadoCargos.DataSource = ""
        dtgNoExisten.DataSource = ""


        ' INICIALIZAR VARIABLES GLOBALES
        contadorf = 0
        FechaDesde = ""
        FechaHasta = ""
        id = ""
        Descripcion = ""
        Fecha = ""
        idDeta = ""
        ClienteCodigoSifco = ""
        ClienteReferencia = ""
        TransaccionNumero = ""
        TransaccionReferencia = ""
        valor = ""


    End Sub
    Private Async Function Enviar() As Task(Of Integer)
        'SECCION TRY CATCH PARA INSERTAR DATOS EN LA TABLA DONDE IRAN TEMPORALMENTE LOS DATOS
        Try
            conexion.EjecutarNonQuery("DELETE FROM dbo.UNI_SIFCO_DATOS_PLANILLA_WEBSERVICE ;")
            For Each fila As DataGridViewRow In dtgListadoPlanilla.Rows
                For Each row As DataGridViewRow In dtgListadoCargos.Rows
                    If fila.Cells(0).Value = row.Cells(6).Value Then
                        'conexion.EjecutarNonQuery("INSERT INTO dbo.UNI_SIFCO_DATOS_PLANILLA_WEBSERVICE (EMPLEADO,CONCEPTO,FECHA_INICIAL,FECHA_FINAL,MONTO,PLANILLA,DESCRIPCION,FECHA,FRECUENCIACODIGO,FRECUENCIANOMBRE,ID,CLIENTECODIGOSIFCO,TRANSACCIONNUMERO) values ('" & row.Cells(2).Value.ToString.Replace(" ", String.Empty) & "','" & row.Cells(4).Value & "','" & FechaDesde & "','" & FechaHasta & "','" & row.Cells(5).Value & "','" & row.Cells(6).Value & "','" & fila.Cells(1).Value & "','" & fila.Cells(2).Value & "','" & fila.Cells(3).Value & "','" & fila.Cells(4).Value & "','" & row.Cells(0).Value & "','" & row.Cells(1).Value & "','" & row.Cells(3).Value & "') ")
                        conexion.EjecutarNonQuery("INSERT INTO dbo.UNI_SIFCO_DATOS_PLANILLA_WEBSERVICE 
(EMPLEADO,
CONCEPTO,
FECHA_INICIAL,
FECHA_FINAL,
MONTO,
PLANILLA,
DESCRIPCION,
FECHA,
FRECUENCIACODIGO,
FRECUENCIANOMBRE,
ID,
CLIENTECODIGOSIFCO,
TRANSACCIONNUMERO) 
values 
('" & row.Cells(2).Value.ToString.Replace(" ", String.Empty) & "',
'" & fila.Cells(4).Value & "',
'" & FechaDesde & "',
'" & FechaHasta & "',
'" & row.Cells(5).Value & "',
'" & row.Cells(6).Value & "',
'" & fila.Cells(1).Value & "',
'" & fila.Cells(2).Value & "',
'" & fila.Cells(3).Value & "',
'" & fila.Cells(4).Value & "',
'" & row.Cells(0).Value & "',
'" & row.Cells(1).Value & "',
'" & row.Cells(3).Value & "') ")
                    End If

                Next
            Next

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try

        'TABLA Y DATAGRIDVIEW PARA VERIFICAR EMPLEADOS NO EXISTENTES
        'tablad = conexion.llenarDataSet("UNI_SIFCO_EMPLEADO_NO_EXISTE;")
        'dtgNoExisten.DataSource = tablad.Tables(0)

        'BLOQUE TRY CATCH PARA LA INSERCCION Y ACTUALIZACION DE LA TABLA A EXACTUS
        Try
            conexion.EjecutarNonQuery("EXEC UNI_SIFCO_PLANILLA_WB_INSERTA;")

            'MsgBox("Datos procesados correctamente", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
            tlsCargarBase.Enabled = False
            tlsDescargaPlanilla.Enabled = False
            tlsRangoFecha.Enabled = False
            tlsLimpiarEditar.Visible = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try
    End Function
    Private Async Sub tlsCargarBase_Click(sender As Object, e As EventArgs) Handles tlsCargarBase.Click

        PEspera5.Visible = True
        tlsDescargaPlanilla.Enabled = False
        tlsCargarBase.Enabled = False
        tlsLimpiarEditar.Visible = False
        tlsRangoFecha.Enabled = False
        BtnSalir.Enabled = False

        Application.DoEvents()
        Dim resultado2 As Integer = Await Task.Run(Function()
                                                       Return Enviar()
                                                   End Function)

        MsgBox("Datos procesados correctamente", MsgBoxStyle.Information, "SIFCO - SOFTLAND")


        PEspera5.Visible = False

        BtnSalir.Enabled = True

    End Sub

    Private Sub dtgNoExisten_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgNoExisten.CellClick
        Dim tabla3 As New DataTable
        Dim dt As New DataTable

        For i As Integer = 0 To dtgListadoCargos.Rows.Count - 1

            If dtgNoExisten.CurrentRow.Cells(0).Value = dtgListadoCargos.Rows(i).Cells(2).Value Then
                idDeta = dtgListadoCargos.Rows(i).Cells(0).Value
                ClienteCodigoSifco = dtgListadoCargos.Rows(i).Cells(1).Value
                ClienteReferencia = dtgListadoCargos.Rows(i).Cells(2).Value
                TransaccionNumero = dtgListadoCargos.Rows(i).Cells(3).Value
                TransaccionReferencia = dtgListadoCargos.Rows(i).Cells(4).Value
                valor = dtgListadoCargos.Rows(i).Cells(5).Value
            End If

        Next



    End Sub

    'Private Sub EncuentraConcepto()

    '    Dim nuevo As String = ""

    '    nuevo = conexion.EjecutarEscalar("Select VALOR FROM dbo.UNI_SIFCO_PARAMETROS WHERE PARAMETRO = '" & TransaccionReferencia & "';")
    '    If nuevo = "" Then

    '        nuevo = conexion.EjecutarEscalar("Select VALOR FROM dbo.UNI_SIFCO_PARAMETROS WHERE PARAMETRO = 'UNISIFCO_CONCEPTO_DEFAULT';")
    '    End If
    '    TransaccionReferencia = nuevo


    'End Sub

    Private Sub BtnSalir_Click(sender As Object, e As EventArgs) Handles BtnSalir.Click
        'Mensaje para regresar al menu
        If (MessageBox.Show("¿Desea regresar al menú?", "SIFCO - SOFTLAND",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            Me.Close()
            Menu_Carga.Show()
        End If
    End Sub
End Class

﻿Imports System
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Web.Script.Serialization
Public Class CargaPlanillas
    Dim planilla As DataSet
    Dim lista As DataSet
    Dim des As String
    Private _errorJSON As String

    Private Function MensajePlanilla(respuesta As Object)

        For Each fila In respuesta

            'Key: es el nombre de la variable que estamos recuperando....
            Select Case (fila.key)
                Case "error"
                    errores = fila.value
                Case "mensajes"
                    mensaje = fila.value
            End Select

        Next
        Return respuesta
    End Function
    Private Async Function CargaPlanillas() As Task(Of Integer)
        Try
            Dim URIC As Uri = New Uri(urlcargaplanilla)
            Dim jsonplanilla As String = String.Empty
            Dim encabezadoJson As String = "{" & vbCrLf & """Planillas"": {"

            Dim encabezadoFila As String = vbCrLf & "  " & """Planillas"": [" & vbCrLf

            Dim finFila As String = vbCrLf & "  " & "]" & vbCrLf & " " & "}"

            Dim encabezadoFila2 As String = """Detalles"": ["

            Dim finFila2 As String = vbCrLf & "      " & "]" & vbCrLf & "    " & "}"

            Dim finJson As String = vbCrLf & "} "

            Dim valorPlanilla As String = String.Empty
            Dim valorDetalle As String = String.Empty

            Dim icontador As Integer = 0
            Dim contador2 As Integer
            If DTGListados.Rows.Count > 0 And dtgDatosPlanilla.Rows.Count > 0 Then
                For Each fila As DataGridViewRow In DTGListados.Rows
                    If icontador > 0 Then
                        valorPlanilla &= ", " & vbCrLf
                    End If


                    valorPlanilla &= "    " & "{" & vbCrLf & "   " &
              "   " & """ID"": """ & fila.Cells(0).Value & """,
      ""Descripcion"": """ & fila.Cells(1).Value & """,
      ""Fecha"": """ & fila.Cells(2).Value & """,
      ""FrecuenciaCodigo"": """ & fila.Cells(3).Value & """,
      ""FrecuenciaNombre"": """ & fila.Cells(4).Value & """,
      " & encabezadoFila2 & ""

                    For Each fila2 As DataGridViewRow In dtgDatosPlanilla.Rows

                        If fila.Cells(0).Value = fila2.Cells(6).Value Then
                            If contador2 > 0 Then
                                valorPlanilla &= ","
                            End If

                            valorDetalle = valorPlanilla & "
        {
          ""ID"": """ & fila2.Cells(0).Value & """,
          ""ClienteCodigoSifco"": """ & fila2.Cells(1).Value & """,
          ""ClienteReferencia"": """ & fila2.Cells(2).Value & """,
          ""TransaccionNumero"": """ & fila2.Cells(3).Value & """,
          ""TransaccionReferencia"": """ & fila2.Cells(4).Value & """,
          ""Valor"": """ & fila2.Cells(5).Value & """ 
        }"
                            contador2 += 1
                            valorPlanilla = valorDetalle
                        Else

                        End If
                    Next
                    valorDetalle = ""
                    valorPlanilla &= finFila2

                    contador2 = 0
                    icontador += 1
                Next

                jsonplanilla = encabezadoJson & encabezadoFila & valorPlanilla & finFila & finJson

                Dim dataplanilla = Encoding.UTF8.GetBytes(jsonplanilla)
                Dim json As New JavaScriptSerializer

                Dim result_post = SendRequest(URIC, dataplanilla, "application/json", "POST")
                Dim respuesta As Object = json.DeserializeObject(result_post)
                MensajePlanilla(respuesta)

                _errorJSON = errores & mensaje
                If _errorJSON = "0" Then
                    Return 1
                    'MsgBox("Datos Procesados Correctamente", MsgBoxStyle.Exclamation, "SIFCO-SOFTLAND")
                Else
                    ' MsgBox(errores & mensaje, MsgBoxStyle.Exclamation, "SIFCO-SOFTLAND")
                    Return 3
                End If

            Else
                'MsgBox("No hay datos a enviar", MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")

                Return 2
            End If

        Catch ex As Exception
            _errorJSON = ex.Message
            Return 0
            ' MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO-SOFTLAND")
        End Try

    End Function
    Private Function SendRequest(uri As Uri, jsonDataBytes As Byte(), contentType As String, method As String)
        Dim res As String
        Dim req As WebRequest = WebRequest.Create(uri)
        req.ContentType = contentType
        req.Headers("Authorization") = access_token
        req.Headers("GENEXUS-AGENT") = agente
        req.Method = method
        req.ContentLength = jsonDataBytes.Length

        Dim stream = req.GetRequestStream()
        stream.Write(jsonDataBytes, 0, jsonDataBytes.Length)
        stream.Close()
        Try
            Dim response = req.GetResponse().GetResponseStream()
            Dim reader As New StreamReader(response)
            res = reader.ReadToEnd()
            reader.Close()
            response.Close()
            Return res

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try
    End Function
    Private Async Function DescargaExactus() As Task(Of Integer)
        Try
            planilla = conexion.llenarDataSet("Exec UNI_SIFCO_CARGA_PLANILLA;")
            lista = conexion.llenarDataSet("Exec UNI_SIFCO_LISTADO_PLANILLA;")


            If planilla.Tables.Count < 0 And lista.Tables.Count < 0 Then
                'dtgDatosPlanilla.DataSource = ""
                'DTGListados.DataSource = ""
                'TlsCargaPWS.Enabled = False
                'TlsDescargaBD.Enabled = True
                'TlsLimpiarTabla.Visible = False
                'MsgBox("No hay datos nuevos", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
                Return 2
            Else
                Return 1
                'MsgBox("Datos Obtenidos", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
                'DTGListados.DataSource = lista.Tables(0)
                'dtgDatosPlanilla.DataSource = planilla.Tables(0)
                'TlsDescargaBD.Enabled = False
                'TlsCargaPWS.Enabled = True
                'TlsLimpiarTabla.Visible = True
            End If

        Catch ex As Exception
            _errorJSON = ex.Message
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
            Return 0
        End Try
    End Function
    Async Sub TlsDescargaBD_Click(sender As Object, e As EventArgs) Handles TlsDescargaBD.Click
        PEspera3.Visible = True
        TlsDescargaBD.Enabled = False
        TlsCargaPWS.Enabled = False
        TlsLimpiarTabla.Visible = False
        BtnCerrar.Enabled = False

        Application.DoEvents()

        Dim resultadoDescarga As Integer = Await Task.Run(Function()
                                                              Return DescargaExactus()
                                                          End Function)
        If resultadoDescarga = 0 Then
            MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")

        ElseIf resultadoDescarga = 1 Then

            DTGListados.DataSource = lista.Tables(0)
            dtgDatosPlanilla.DataSource = planilla.Tables(0)
            MsgBox(dtgDatosPlanilla.Rows.Count & " Datos Obtenidos", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
            TlsDescargaBD.Enabled = False
            TlsCargaPWS.Enabled = True
            TlsLimpiarTabla.Visible = True

        ElseIf resultadoDescarga = 2 Then
            MsgBox("No hay datos nuevos", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
            dtgDatosPlanilla.DataSource = ""
            DTGListados.DataSource = ""
            TlsCargaPWS.Enabled = False
            TlsDescargaBD.Enabled = True
            TlsLimpiarTabla.Visible = False

        End If

        'Fin de la pantalla de espera
        PEspera3.Visible = False
        BtnCerrar.Enabled = True


    End Sub
    Async Sub TlsCargaPWS_Click(sender As Object, e As EventArgs) Handles TlsCargaPWS.Click

        PEspera3.Visible = True
        TlsDescargaBD.Enabled = False
        TlsCargaPWS.Enabled = False
        TlsLimpiarTabla.Visible = False
        BtnCerrar.Enabled = False

        Application.DoEvents()

        Dim resultadoCarga As Integer = Await Task.Run(Function()
                                                           Return CargaPlanillas()
                                                       End Function)

        If resultadoCarga = 0 Then
            MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")

        ElseIf resultadoCarga = 1 Then
            MsgBox(dtgDatosPlanilla.Rows.Count & " Datos Procesados Correctamente", MsgBoxStyle.Information, "SIFCO-SOFTLAND")
            TlsCargaPWS.Enabled = False
            TlsDescargaBD.Enabled = False
            TlsLimpiarTabla.Visible = True

        ElseIf resultadoCarga = 2 Then
            MsgBox("No hay datos a enviar", MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
            TlsCargaPWS.Enabled = False
            TlsDescargaBD.Enabled = True
            TlsLimpiarTabla.Visible = False
        Else
            MsgBox(errores & mensaje, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")
            TlsCargaPWS.Enabled = False
            TlsDescargaBD.Enabled = False
            TlsLimpiarTabla.Visible = True
        End If

        'Fin de la pantalla de espera
        PEspera3.Visible = False
        BtnCerrar.Enabled = True
    End Sub
    Private Sub TlsLimpiarTabla_Click(sender As Object, e As EventArgs) Handles TlsLimpiarTabla.Click
        dtgDatosPlanilla.DataSource = ""
        DTGListados.DataSource = ""

        TlsDescargaBD.Enabled = True
        TlsCargaPWS.Enabled = False
        TlsLimpiarTabla.Visible = False
        planilla.Reset()

    End Sub


    Private Sub CargaPlanillas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label3.Text = "Versión " & strVersion
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles BtnCerrar.Click
        If (MessageBox.Show("¿Desea regresar al menú?", "SIFCO - SOFTLAND",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            Me.Close()
            Menu_Carga.Show()
        End If
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Menu_Carga
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Menu_Carga))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.BtnSalir = New System.Windows.Forms.Button()
        Me.BtnConsultaEmail = New System.Windows.Forms.Button()
        Me.BtnCambioEstado = New System.Windows.Forms.Button()
        Me.BtnCargaPlanilla = New System.Windows.Forms.Button()
        Me.BtnObtenerDT = New System.Windows.Forms.Button()
        Me.BtnCargaMasiva = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(88, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(768, 57)
        Me.Panel1.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label1.Image = CType(resources.GetObject("Label1.Image"), System.Drawing.Image)
        Me.Label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label1.Location = New System.Drawing.Point(20, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 44)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Menu"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.Panel4.Controls.Add(Me.Label2)
        Me.Panel4.Controls.Add(Me.Panel5)
        Me.Panel4.Controls.Add(Me.BtnSalir)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 456)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(768, 52)
        Me.Panel4.TabIndex = 114
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(15, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 23)
        Me.Label2.TabIndex = 2
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel5
        '
        Me.Panel5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel5.BackColor = System.Drawing.Color.FromArgb(CType(CType(175, Byte), Integer), CType(CType(175, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.Panel5.Location = New System.Drawing.Point(-4, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(784, 1)
        Me.Panel5.TabIndex = 1
        '
        'BtnSalir
        '
        Me.BtnSalir.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnSalir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSalir.FlatAppearance.BorderSize = 0
        Me.BtnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSalir.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSalir.Image = CType(resources.GetObject("BtnSalir.Image"), System.Drawing.Image)
        Me.BtnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnSalir.Location = New System.Drawing.Point(672, 7)
        Me.BtnSalir.Name = "BtnSalir"
        Me.BtnSalir.Size = New System.Drawing.Size(80, 35)
        Me.BtnSalir.TabIndex = 0
        Me.BtnSalir.Text = "Salir"
        Me.BtnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnSalir.UseVisualStyleBackColor = True
        '
        'BtnConsultaEmail
        '
        Me.BtnConsultaEmail.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnConsultaEmail.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnConsultaEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnConsultaEmail.Image = CType(resources.GetObject("BtnConsultaEmail.Image"), System.Drawing.Image)
        Me.BtnConsultaEmail.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnConsultaEmail.Location = New System.Drawing.Point(248, 343)
        Me.BtnConsultaEmail.Name = "BtnConsultaEmail"
        Me.BtnConsultaEmail.Size = New System.Drawing.Size(261, 76)
        Me.BtnConsultaEmail.TabIndex = 116
        Me.BtnConsultaEmail.Text = "Actualización de Email SIFCO-SOFTLAND"
        Me.BtnConsultaEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnConsultaEmail.UseVisualStyleBackColor = True
        '
        'BtnCambioEstado
        '
        Me.BtnCambioEstado.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnCambioEstado.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnCambioEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCambioEstado.Image = CType(resources.GetObject("BtnCambioEstado.Image"), System.Drawing.Image)
        Me.BtnCambioEstado.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnCambioEstado.Location = New System.Drawing.Point(436, 226)
        Me.BtnCambioEstado.Name = "BtnCambioEstado"
        Me.BtnCambioEstado.Size = New System.Drawing.Size(259, 76)
        Me.BtnCambioEstado.TabIndex = 115
        Me.BtnCambioEstado.Text = "Cambio de Estados SIFCO-SOFTLAND"
        Me.BtnCambioEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnCambioEstado.UseVisualStyleBackColor = True
        '
        'BtnCargaPlanilla
        '
        Me.BtnCargaPlanilla.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnCargaPlanilla.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnCargaPlanilla.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCargaPlanilla.Image = CType(resources.GetObject("BtnCargaPlanilla.Image"), System.Drawing.Image)
        Me.BtnCargaPlanilla.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnCargaPlanilla.Location = New System.Drawing.Point(436, 105)
        Me.BtnCargaPlanilla.Name = "BtnCargaPlanilla"
        Me.BtnCargaPlanilla.Size = New System.Drawing.Size(259, 76)
        Me.BtnCargaPlanilla.TabIndex = 6
        Me.BtnCargaPlanilla.Text = "Carga Planilla SOFTLAND-SIFCO"
        Me.BtnCargaPlanilla.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnCargaPlanilla.UseVisualStyleBackColor = True
        '
        'BtnObtenerDT
        '
        Me.BtnObtenerDT.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnObtenerDT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnObtenerDT.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnObtenerDT.Image = CType(resources.GetObject("BtnObtenerDT.Image"), System.Drawing.Image)
        Me.BtnObtenerDT.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnObtenerDT.Location = New System.Drawing.Point(57, 226)
        Me.BtnObtenerDT.Name = "BtnObtenerDT"
        Me.BtnObtenerDT.Size = New System.Drawing.Size(261, 76)
        Me.BtnObtenerDT.TabIndex = 5
        Me.BtnObtenerDT.Text = "Descarga Descuentos SIFCO-SOFTLAND"
        Me.BtnObtenerDT.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnObtenerDT.UseVisualStyleBackColor = True
        '
        'BtnCargaMasiva
        '
        Me.BtnCargaMasiva.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnCargaMasiva.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnCargaMasiva.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCargaMasiva.Image = CType(resources.GetObject("BtnCargaMasiva.Image"), System.Drawing.Image)
        Me.BtnCargaMasiva.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnCargaMasiva.Location = New System.Drawing.Point(57, 105)
        Me.BtnCargaMasiva.Name = "BtnCargaMasiva"
        Me.BtnCargaMasiva.Size = New System.Drawing.Size(261, 76)
        Me.BtnCargaMasiva.TabIndex = 3
        Me.BtnCargaMasiva.Text = "Carga Masiva SOFTLAND-SIFCO"
        Me.BtnCargaMasiva.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnCargaMasiva.UseVisualStyleBackColor = True
        '
        'Menu_Carga
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(768, 508)
        Me.Controls.Add(Me.BtnConsultaEmail)
        Me.Controls.Add(Me.BtnCambioEstado)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.BtnCargaPlanilla)
        Me.Controls.Add(Me.BtnObtenerDT)
        Me.Controls.Add(Me.BtnCargaMasiva)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Menu_Carga"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SIFCO -SOFTLAND"
        Me.Panel1.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents BtnCargaMasiva As Button
    Friend WithEvents BtnObtenerDT As Button
    Friend WithEvents BtnCargaPlanilla As Button
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents BtnSalir As Button
    Friend WithEvents BtnCambioEstado As Button
    Friend WithEvents BtnConsultaEmail As Button
    Friend WithEvents Label2 As Label
End Class

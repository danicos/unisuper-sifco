﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Web.Script.Serialization

Public Class ConsultaCorreo
    Dim tabla As New DataTable
    Private _errorJSON As String
    Private Function RecorreCadena(respuesta As Object)

        For Each fila In respuesta
            Dim res As New JavaScriptSerializer
            Dim val As String = res.Serialize(fila)
            Dim valuedes As Object = res.DeserializeObject(val)
            For Each fila2 In valuedes
                Select Case (fila2.key)
                    Case "Key"
                        Select Case (fila.key)
                            Case "Clientes"
                                RecorreCadena(fila.value)
                        End Select
                    Case "CodigoCliente"
                        CodigoC = fila2.value
                    Case "NombreCompleto"
                        Nombre = fila2.value
                    Case "CodigoReferencia"
                        CodigoReferencia = fila2.value
                    Case "DireccionEMailPrincipal"
                        EmailPrincipal = fila2.value
                    Case "DireccionEMailSecundario"
                        EmailSecundario = fila2.value
                    Case "ExcluirDeMensajesDeCorreo"
                        ExcluirMensajes = fila2.value
                        If tabla.Rows.Count = 0 Then
                            tabla.Columns.Add("CodigoCliente")
                            tabla.Columns.Add("NombreCompleto")
                            tabla.Columns.Add("CodigoReferencia")
                            tabla.Columns.Add("DireccionEMailPrincipal")
                            tabla.Columns.Add("DireccionEMailSecundario")
                            tabla.Columns.Add("ExcluirDeMensajesDeCorreo")
                        End If

                        Dim Renglon As DataRow = tabla.NewRow()
                        Renglon(0) = CodigoC
                        Renglon(1) = Nombre
                        Renglon(2) = CodigoReferencia
                        Renglon(3) = EmailPrincipal
                        Renglon(4) = EmailSecundario
                        Renglon(5) = ExcluirMensajes
                        tabla.Rows.Add(Renglon)
                End Select

            Next
        Next
        Return respuesta
    End Function
    Private Async Function MostrarDatos() As Task(Of Integer)
        Dim URL As Uri = New Uri(urlconsultaemail)
        Dim JsonString As String = String.Empty
        Dim cadena As String = String.Empty

        cadena = "{}"

        Dim data = Encoding.UTF8.GetBytes(cadena)

        Dim result_post = SendRequest(URL, data, "application/json", "POST")

        Dim json As New JavaScriptSerializer
        Try

            Dim respuesta As Object = json.DeserializeObject(result_post)
            RecorreCadena(respuesta)


        Catch ex As Exception
            Return 0
            _errorJSON = ex.Message
            'MsgBox(_errorJSON, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try

        If tabla.Rows.Count = 0 Then

            Return 2
        Else
            Return 1

        End If

    End Function
    Private Function SendRequest(uri As Uri, jsonDataBytes As Byte(), contentType As String, method As String)
        Dim res As String
        Dim req As WebRequest = WebRequest.Create(uri)
        req.ContentType = contentType
        req.Headers("Authorization") = access_token
        req.Headers("GENEXUS-AGENT") = agente
        req.Method = method
        req.ContentLength = jsonDataBytes.Length

        Dim stream = req.GetRequestStream()
        stream.Write(jsonDataBytes, 0, jsonDataBytes.Length)
        stream.Close()
        Try
            Dim response = req.GetResponse().GetResponseStream()
            Dim reader As New StreamReader(response)
            res = reader.ReadToEnd()
            reader.Close()
            response.Close()
            Return res
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try
    End Function
    Async Sub TlsBuscarCampos_Click(sender As Object, e As EventArgs) Handles TlsBuscarCampos.Click
        PEspera4.Visible = True
        TlsBuscarCampos.Enabled = False
        TlsPasarCampo.Enabled = False
        BtnCerrar.Enabled = False

        Application.DoEvents()

        'Se llama a la Funcion Carga empleados

        Dim resultado As Integer = Await Task.Run(Function()
                                                      Return MostrarDatos()
                                                  End Function)

        If resultado = 0 Then
            MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")
        ElseIf resultado = 1 Then
            DtgCamposExactus.DataSource = tabla.Copy
            MsgBox("Datos Obtenidos ", MsgBoxStyle.Information, "SIFCO - SOFTLAND")

            TlsLimpiar.Visible = True
            TlsPasarCampo.Enabled = True
            TlsBuscarCampos.Enabled = False
        ElseIf resultado = 2 Then
            MsgBox("No existen datos", MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
            TlsBuscarCampos.Enabled = True
            TlsLimpiar.Visible = False
            TlsPasarCampo.Enabled = False
        End If


        PEspera4.Visible = False
        BtnCerrar.Enabled = True
    End Sub

    Private Sub TlsLimpiar_Click(sender As Object, e As EventArgs) Handles TlsLimpiar.Click
        tabla.Reset()
        DtgCamposExactus.DataSource = ""
        TlsBuscarCampos.Enabled = True
        TlsPasarCampo.Enabled = False
        TlsLimpiar.Visible = False
    End Sub
    Private Async Function PasarEmail() As Task(Of Integer)
        Try
            conexion.EjecutarNonQuery("DELETE FROM dbo.UNI_SIFCO_EMAIL ;")
            For Each fila As DataGridViewRow In DtgCamposExactus.Rows

                conexion.EjecutarNonQuery("INSERT INTO dbo.UNI_SIFCO_EMAIL (CODIGO_CLIENTE,NOMBRE_COMPLETO,CODIGO_REFERENCIA,EMAIL_PRINCIPAL,EMAIL_SECUNDARIA,EXCLUIR_MENSAJES) values ('" & fila.Cells(0).Value & "','" & fila.Cells(1).Value & "','" & fila.Cells(2).Value & "','" & fila.Cells(3).Value & "','" & fila.Cells(4).Value & "','" & fila.Cells(5).Value & "') SELECT * FROM dbo.UNI_SIFCO_EMAIL ORDER BY CODIGO_CLIENTE ")
            Next

        Catch ex As Exception
            _errorJSON = ex.Message
            Return 0
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try
        'BLOQUE TRY CATCH PARA LA INSERCCION Y ACTUALIZACION DE LA TABLA A EXACTUS
        Try

            conexion.EjecutarNonQuery("EXEC UNI_SIFCO_ACTUALIZA_EMAIL;")
            Return 1


        Catch ex As Exception
            _errorJSON = ex.Message
            Return 2


        End Try
    End Function

    Async Sub TlsPasarCampo_Click(sender As Object, e As EventArgs) Handles TlsPasarCampo.Click
        PEspera4.Visible = True
        TlsBuscarCampos.Enabled = False
        TlsPasarCampo.Enabled = False
        BtnCerrar.Enabled = False

        Application.DoEvents()

        'Se llama a la Funcion Carga empleados

        Dim resultado As Integer = Await Task.Run(Function()
                                                      Return PasarEmail()
                                                  End Function)

        If resultado = 0 Then
            MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")
        ElseIf resultado = 1 Then
            MsgBox("Datos procesados correctamente", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
            TlsPasarCampo.Enabled = False
            TlsLimpiar.Visible = True
            TlsBuscarCampos.Enabled = False
        ElseIf resultado = 2 Then
            MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")
            TlsBuscarCampos.Enabled = True
            TlsLimpiar.Visible = False
            TlsPasarCampo.Enabled = False
        End If


        PEspera4.Visible = False
        BtnCerrar.Enabled = True

    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles BtnCerrar.Click
        'Mensaje para regresar al menu
        If (MessageBox.Show("¿Desea regresar al menú?", "SIFCO - SOFTLAND",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            Me.Close()
            Menu_Carga.Show()
        End If
    End Sub

    Private Sub ConsultaCorreo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label3.Text = "Versión " & strVersion
    End Sub
End Class
﻿Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Web.Script.Serialization

Public Class CambioEstado
    Dim activos As DataSet
    Dim inactivos As DataSet
    Private _errorJSON As String

    Private Function MensajeCambio(respuesta As Object)

        For Each fila In respuesta
            Dim res As New JavaScriptSerializer
            Dim val As String = res.Serialize(fila)
            Dim valuedes As Object = res.DeserializeObject(val)
            'Key: es el nombre de la variable que estamos recuperando....
            For Each fila2 In valuedes
                Select Case (fila2.key)
                    Case "Key"
                        Select Case (fila.key)
                            Case "Messages"
                                MensajeCambio(fila.value)
                        End Select
                    Case "Id"
                        Idm = fila2.value
                    Case "Type"
                        Type = fila2.value
                    Case "Description"
                        Description = fila2.value
                End Select

            Next

        Next
        Return respuesta
    End Function
    Private Async Function Buscar() As Task(Of Integer)
        Try
            'Muestra los empleados inactivos en la tabla dbo.UNI_SIFCO_EMPLEADOS_CONTROL, pero se encuentran
            'activos en la tabla UNISUP.EMPLEADO
            activos = New DataSet
            activos = conexion.llenarDataSet("Exec UNI_SIFCO_BUSCA_ESTADO_ACT;")

            'Muestra los empleados activos en la tabla  dbo.UNI_SIFCO_EMPLEADOS_CONTROL, pero se encuentran
            'inactivos en la tabla UNISUP.EMPLEADO
            inactivos = New DataSet
            inactivos = conexion.llenarDataSet("Exec UNI_SIFCO_BUSCA_ESTADO_INAC;")

            If activos.Tables(0).Rows.Count > 0 Or inactivos.Tables(0).Rows.Count > 0 Then

                Return 1
            Else
                Return 2


            End If
        Catch ex As Exception
            Return 0
            _errorJSON = ex.Message

        End Try

    End Function
    Async Sub TlsBuscarDatos_Click(sender As Object, e As EventArgs) Handles TlsBuscarDatos.Click
        PEspera2.Visible = True
        TlsBuscarDatos.Enabled = False
        TlsRealizarCambios.Enabled = False
        TlsLimpiar.Visible = False
        BtnCerrar.Enabled = False

        Dim resultado As Integer = Await Task.Run(Function()
                                                      Return Buscar()
                                                  End Function)

        If resultado = 0 Then
            MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO-SOFTLAND")
        ElseIf resultado = 1 Then
            DtgActivos.DataSource = activos.Tables(0)
            DtgInactivos.DataSource = inactivos.Tables(0)
            MsgBox(activos.Tables(0).Rows.Count & " Activos" & ", " & inactivos.Tables(0).Rows.Count & " Inactivos ", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
            TlsBuscarDatos.Enabled = False
            TlsRealizarCambios.Enabled = True
            TlsLimpiar.Visible = True

        ElseIf resultado = 2 Then
            DtgActivos.DataSource = activos.Tables
            DtgInactivos.DataSource = inactivos.Tables
            TlsBuscarDatos.Enabled = True
            TlsLimpiar.Visible = False
            TlsRealizarCambios.Enabled = False
            MsgBox("No hay datos nuevos", MsgBoxStyle.Information, "SIFCO - SOFTLAND")
        Else
            MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO - SOFTLAND")
        End If

        'Fin de la pantalla de espera
        PEspera2.Visible = False

        BtnCerrar.Enabled = True

    End Sub
    Private Async Function CargarCambios() As Task(Of Integer)

        Try
            Dim URI As Uri = New Uri(urlcambioestado)
            Dim jsonString As String = String.Empty
            Dim CadenaCambio As String = String.Empty
            Dim encabezado As String = "{"
            Dim finCadena As String = "}"

            If activos.Tables(0).Rows.Count > 0 Or inactivos.Tables(0).Rows.Count > 0 Then

                For Each filaActivo As DataRow In activos.Tables(0).Rows
                    Idm = ""
                    Type = ""
                    Description = ""
                    CadenaCambio = """Operacion"": """ & OperacionAct & """,
                              ""FormaIdentificar"": " & FormaIdentificar & ",
                              ""Identificador"": """ & filaActivo("EMPLEADO") & """"

                    jsonString = encabezado & CadenaCambio & finCadena
                    Dim data = Encoding.UTF8.GetBytes(jsonString)

                    Dim result_post = SendRequest(URI, data, "application/json", "POST")

                    Dim json As New JavaScriptSerializer
                    Dim respuesta As Object = json.DeserializeObject(result_post)
                    MensajeCambio(respuesta)

                    If Idm = "ErrorCliente" Then
                        '_errorJSON = Idm & " " & Type & vbCrLf & Description
                        'Return 3
                        filaActivo("RESPUESTA") = Idm & " " & Type & vbCrLf & Description
                    Else
                        '_errorJSON = Idm & " " & Type & vbCrLf & Description
                        'Return 1
                        filaActivo("RESPUESTA") = "ok"
                    End If

                Next

                For Each fila2 As DataRow In inactivos.Tables(0).Rows
                    CadenaCambio = """Operacion"": " & OperacionInac & ",
                              ""FormaIdentificar"": " & FormaIdentificar & ",
                              ""Identificador"": " & fila2("EMPLEADO") & ""
                    jsonString = encabezado & CadenaCambio & finCadena
                    Dim data = Encoding.UTF8.GetBytes(jsonString)
                    Dim result_post = SendRequest(URI, data, "application/json", "POST")
                    Dim json As New JavaScriptSerializer
                    Dim respuesta As Object = json.DeserializeObject(result_post)
                    MensajeCambio(respuesta)
                    If Idm = "ErrorCliente" Then
                        fila2("RESPUESTA") = Idm & " " & Type & vbCrLf & Description
                        '_errorJSON = Idm & " " & Type & vbCrLf & Description
                        'MsgBox(_errorJSON, MsgBoxStyle.Critical, "SIFCO - SOFTLAND")
                        'Return 3
                    Else
                        fila2("RESPUESTA") = "ok"
                        TlsRealizarCambios.Enabled = False
                        TlsBuscarDatos.Enabled = False
                        TlsLimpiar.Visible = True
                        '_errorJSON = Idm & " " & Type & vbCrLf & Description
                        'MsgBox(Idm & " " & Type & vbCrLf & Description, MsgBoxStyle.Information, "SIFCO - SOFTLAND")
                        'Return 1
                    End If

                Next

            Else
                jsonString = encabezado & CadenaCambio & finCadena
                Dim data = Encoding.UTF8.GetBytes(jsonString)
                Dim result_post = SendRequest(URI, data, "application/json", "POST")
                Dim json As New JavaScriptSerializer
                Dim respuesta As Object = json.DeserializeObject(result_post)
                MensajeCambio(respuesta)
                MsgBox("No hay datos a enviar", MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
                TlsBuscarDatos.Enabled = True
                TlsRealizarCambios.Enabled = False
                TlsLimpiar.Visible = False

            End If

        Catch ex As Exception
            '_errorJSON = ex.Message
            'Return 0
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")

        End Try

    End Function
    Private Async Function Actualizar() As Task(Of Integer)
        Dim tablaAct As New DataTable
        Dim tablaInac As New DataTable
        tablaAct = activos.Tables(0)
        tablaInac = inactivos.Tables(0)
        Try
            conexion.EjecutarNonQuery("DELETE FROM dbo.UNI_SIFCO_ESTADO_CONTROL_INACT ;")
            conexion.EjecutarNonQuery("DELETE FROM dbo.UNI_SIFCO_ESTADO_CONTROL_ACT ;")

            For Each Activos As DataRow In tablaAct.Rows
                conexion.EjecutarNonQuery("INSERT INTO dbo.UNI_SIFCO_ESTADO_CONTROL_ACT(EMPLEADO,NOMBRE,RESPUESTA) values ('" & Activos(0) & "','" & Activos(1) & "','" & Activos(2) & "') ")
            Next
            For Each Inactivos As DataRow In tablaInac.Rows
                conexion.EjecutarNonQuery("INSERT INTO dbo.UNI_SIFCO_ESTADO_CONTROL_INACT(EMPLEADO,NOMBRE,RESPUESTA) values ('" & Inactivos(0) & "','" & Inactivos(1) & "','" & Inactivos(2) & "') ")
            Next

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try

        'UPDATE DE LOS INACTIVOS EN UNISUP.EMPLEADOS Y ACTIVOS EN dbo.UNI_SIFCO_EMPLEADO_CONTROL

        conexion.EjecutarNonQuery("UPDATE ec
SET EstadoCliente ='I' 
FROM dbo.UNI_SIFCO_EMPLEADOS_CONTROL ec 
INNER JOIN  dbo.UNI_SIFCO_ESTADO_CONTROL_INACT i
ON ec.CodigoReferencia = i.EMPLEADO
WHERE i.RESPUESTA = 'ok'")

        'UPDATE DE LOS ACTIVOS EN UNISUP.EMPLEADOS Y DESACTIVADOS EN dbo.UNI_SIFCO_EMPLEADO_CONTROL

        conexion.EjecutarNonQuery("UPDATE ec
SET EstadoCliente ='A' 
FROM dbo.UNI_SIFCO_EMPLEADOS_CONTROL ec 
INNER JOIN  dbo.UNI_SIFCO_ESTADO_CONTROL_ACT i
ON ec.CodigoReferencia = i.EMPLEADO
WHERE i.RESPUESTA = 'ok'")


    End Function
    Private Function SendRequest(uri As Uri, jsonDataBytes As Byte(), contentType As String, method As String)
        Dim res As String
        Dim req As WebRequest = WebRequest.Create(uri)
        req.ContentType = contentType
        req.Headers("Authorization") = access_token
        req.Headers("GENEXUS-AGENT") = "SmartDevice Application"
        req.Method = method
        req.ContentLength = jsonDataBytes.Length

        Dim stream = req.GetRequestStream()
        stream.Write(jsonDataBytes, 0, jsonDataBytes.Length)
        stream.Close()
        Try
            Dim response = req.GetResponse().GetResponseStream()
            Dim reader As New StreamReader(response)
            res = reader.ReadToEnd()
            reader.Close()
            response.Close()
            Return res
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "SIFCO - SOFTLAND")
        End Try
    End Function
    Private Sub TlsLimpiar_Click(sender As Object, e As EventArgs) Handles TlsLimpiar.Click
        DtgActivos.DataSource = ""
        DtgInactivos.DataSource = ""

        TlsRealizarCambios.Enabled = False
        TlsBuscarDatos.Enabled = True
        TlsLimpiar.Visible = False

        activos.Reset()
        inactivos.Reset()
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles BtnCerrar.Click
        'Mensaje para regresar al menu
        If (MessageBox.Show("¿Desea regresar al menú?", "SIFCO - SOFTLAND",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            Me.Close()
            Menu_Carga.Show()
        End If
    End Sub

    Async Sub TlsRealizarCambios_Click(sender As Object, e As EventArgs) Handles TlsRealizarCambios.Click
        PEspera2.Visible = True
        TlsBuscarDatos.Enabled = False
        TlsRealizarCambios.Enabled = False
        TlsLimpiar.Visible = False
        BtnCerrar.Enabled = False

        Dim resultado As Integer = Await Task.Run(Function()
                                                      Return CargarCambios()
                                                  End Function)

        Dim resultado2 As Integer = Await Task.Run(Function()
                                                       Return Actualizar()
                                                   End Function)

        MsgBox("Proceso finalizado", MsgBoxStyle.Information, "SIFCO - SOFTLAND")


        'Fin de la pantalla de espera
        PEspera2.Visible = False
        TlsLimpiar.Visible = True
        BtnCerrar.Enabled = True

    End Sub

    Private Sub CambioEstado_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label3.Text = "Versión " & strVersion
    End Sub
End Class
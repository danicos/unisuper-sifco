﻿Public Class Menu_Carga
    Dim passwordt As String
    Private Sub BtnCargaMasiva_Click(sender As Object, e As EventArgs) Handles BtnCargaMasiva.Click
        Me.Hide()
        CargaMasiva.ShowDialog()
        Me.Show()
        CargaMasiva.Close()
    End Sub

    Private Sub Menu_Carga_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Label2.Text = "Versión " & strVersion
        'Busca la cadena de conexion para la base de datos
        conexion.BuscarConexionEnXml("cnSQL.xml", Application.StartupPath)

        Try
            ClientId = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'ClientId'")
            ClientSecret = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'ClientSecret'")
            Grant_Type = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'Grant_Type'")
            Scope = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'Scope'")
            Username = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'Username'")
            passwordt = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'Password'")
            Password = Crypto.Crypto.DesEncriptar(passwordt)
            urlautenticacion = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlAutenticacion'")
            urlcarga = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlCarga'")
            urldescargadescuentos = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlDescargaDescuento'")
            urlcargaplanilla = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlCargaPlanilla'")
            urlcambioestado = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlCambioEstado'")
            urlconsultaemail = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'UrlConsultaEmail'")
            CMAFormatoID = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'CMAFormatoID'")
            CMArchivoTC = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'CMArchivoTC'")
            FormaIdentificar = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'FormaIdentificar'")
            OperacionAct = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'OperacionAct'")
            OperacionInac = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'OperacionInac'")
            agente = conexion.EjecutarEscalar("Select VALOR From dbo.UNI_SIFCO_PARAMETROS where PARAMETRO = 'GENEXUS-AGENT'")
            autenticacion()

        Catch ex As Exception
            'Si no hay informacion en la tabla de la autenticacion, manda un mensaje
            MsgBox(ex.Message, MsgBoxStyle.Critical, "SIFCO - SOFTLAND")


        End Try

    End Sub


    Private Sub BtnObtenerDT_Click(sender As Object, e As EventArgs) Handles BtnObtenerDT.Click

        Me.Hide()
        DescargaDescuentos.ShowDialog()
        Me.Show()
        DescargaDescuentos.Close()
    End Sub

    Private Sub BtnCargaPlanilla_Click(sender As Object, e As EventArgs) Handles BtnCargaPlanilla.Click

        Me.Hide()
        CargaPlanillas.ShowDialog()
        Me.Show()
        CargaPlanillas.Close()

    End Sub

    Private Sub BtnSalir_Click(sender As Object, e As EventArgs) Handles BtnSalir.Click
        'Mensaje para salir
        If (MessageBox.Show("¿Desea salir de la aplicaion?", "SIFCO - SOFTLAND",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            End

        End If
    End Sub

    Private Sub BtnCambioEstado_Click(sender As Object, e As EventArgs) Handles BtnCambioEstado.Click

        Me.Hide()
        CambioEstado.ShowDialog()
        Me.Show()
        CambioEstado.Close()

    End Sub

    Private Sub BtnConsultaEmail_Click(sender As Object, e As EventArgs) Handles BtnConsultaEmail.Click
        ' autenticacion()
        Me.Hide()
        ConsultaCorreo.ShowDialog()
        Me.Show()
        ConsultaCorreo.Close()
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class